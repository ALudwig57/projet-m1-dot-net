namespace CodeFirstTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Playlists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Playlist_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Playlists", t => t.Playlist_ID)
                .Index(t => t.Playlist_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Videos", "Playlist_ID", "dbo.Playlists");
            DropIndex("dbo.Videos", new[] { "Playlist_ID" });
            DropTable("dbo.Videos");
            DropTable("dbo.Playlists");
        }
    }
}
