﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstTest
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new MyContext())
            {
                db.Database.Delete();
            }
        }
    }

    public class Playlist
    {
        public int ID { get; set; }
        public string Title { get; set; }

        public List<Video> Videos { get; set; }
    }

    public class Video
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }



    public class MyContext : DbContext
    {
        public DbSet<Video> Videos { get; set; }
        public DbSet<Playlist> Playlists { get; set; }
    }
}
