namespace TestCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnneeUnivs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        year = c.String(),
                        S1ID = c.Int(nullable: false),
                        S2ID = c.Int(nullable: false),
                        Diplome_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Diplomes", t => t.Diplome_ID)
                .ForeignKey("dbo.Semestres", t => t.S1ID, cascadeDelete: false)
                .ForeignKey("dbo.Semestres", t => t.S2ID, cascadeDelete: false)
                .Index(t => t.S1ID)
                .Index(t => t.S2ID)
                .Index(t => t.Diplome_ID);
            
            CreateTable(
                "dbo.Diplomes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Semestres",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        AnneeUniv_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AnneeUnivs", t => t.AnneeUniv_ID)
                .Index(t => t.AnneeUniv_ID);
            
            CreateTable(
                "dbo.Playlists",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Videos",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Description = c.String(),
                        Playlist_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Playlists", t => t.Playlist_ID)
                .Index(t => t.Playlist_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Videos", "Playlist_ID", "dbo.Playlists");
            DropForeignKey("dbo.AnneeUnivs", "S2ID", "dbo.Semestres");
            DropForeignKey("dbo.AnneeUnivs", "S1ID", "dbo.Semestres");
            DropForeignKey("dbo.Semestres", "AnneeUniv_ID", "dbo.AnneeUnivs");
            DropForeignKey("dbo.AnneeUnivs", "Diplome_ID", "dbo.Diplomes");
            DropIndex("dbo.Videos", new[] { "Playlist_ID" });
            DropIndex("dbo.Semestres", new[] { "AnneeUniv_ID" });
            DropIndex("dbo.AnneeUnivs", new[] { "Diplome_ID" });
            DropIndex("dbo.AnneeUnivs", new[] { "S2ID" });
            DropIndex("dbo.AnneeUnivs", new[] { "S1ID" });
            DropTable("dbo.Videos");
            DropTable("dbo.Playlists");
            DropTable("dbo.Semestres");
            DropTable("dbo.Diplomes");
            DropTable("dbo.AnneeUnivs");
        }
    }
}
