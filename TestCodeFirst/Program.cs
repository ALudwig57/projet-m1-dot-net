﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MyContext db = new MyContext())
            {
                Video meVideo = new Video
                {
                    Title = "Hello world Entity Framework",
                    Description = "Learn Entity Framework"
                };
                db.Videos.Add(meVideo);
                db.SaveChanges();

                Playlist mePlaylist = new Playlist();
                mePlaylist.Title = "Entity Framework";
                mePlaylist.Videos = new List<Video>();
                mePlaylist.Videos.Add(meVideo);
                db.Playlists.Add(mePlaylist);
                db.SaveChanges();
                Console.WriteLine(meVideo.Playlist.Title);
                Console.ReadLine();

            }

        }

        public class Playlist
        {
            public int ID { get; set; }
            public string Title { get; set; }
            public List<Video> Videos { get; set; }
        }

        public class Video
        {
            public int ID { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
            public Playlist Playlist { get; set; }
        }

        public class MyContext : DbContext
        {
            public DbSet<Playlist> Playlists { get; set; }
            public DbSet<Video> Videos { get; set; }

        }
    }
}
