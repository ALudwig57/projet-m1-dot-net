namespace CodeFirstConventionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnneeUnivs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        year = c.String(),
                        S1ID = c.Int(nullable: false),
                        S2ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Semestres", t => t.S1ID, cascadeDelete: false)
                .ForeignKey("dbo.Semestres", t => t.S2ID, cascadeDelete: false)
                .Index(t => t.S1ID)
                .Index(t => t.S2ID);
            
            CreateTable(
                "dbo.Semestres",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.Int(nullable: false),
                        AnneeUniv_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AnneeUnivs", t => t.AnneeUniv_ID)
                .Index(t => t.AnneeUniv_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnneeUnivs", "S2ID", "dbo.Semestres");
            DropForeignKey("dbo.AnneeUnivs", "S1ID", "dbo.Semestres");
            DropForeignKey("dbo.Semestres", "AnneeUniv_ID", "dbo.AnneeUnivs");
            DropIndex("dbo.Semestres", new[] { "AnneeUniv_ID" });
            DropIndex("dbo.AnneeUnivs", new[] { "S2ID" });
            DropIndex("dbo.AnneeUnivs", new[] { "S1ID" });
            DropTable("dbo.Semestres");
            DropTable("dbo.AnneeUnivs");
        }
    }
}
