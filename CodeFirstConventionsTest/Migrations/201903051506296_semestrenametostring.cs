namespace CodeFirstConventionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class semestrenametostring : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Semestres", "name", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Semestres", "name", c => c.Int(nullable: false));
        }
    }
}
