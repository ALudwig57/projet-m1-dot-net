namespace CodeFirstConventionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_diplome : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Diplomes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        libelle = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.AnneeUnivs", "AnneeDiplomeID", c => c.Int());
            CreateIndex("dbo.AnneeUnivs", "AnneeDiplomeID");
            AddForeignKey("dbo.AnneeUnivs", "AnneeDiplomeID", "dbo.Diplomes", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnneeUnivs", "AnneeDiplomeID", "dbo.Diplomes");
            DropIndex("dbo.AnneeUnivs", new[] { "AnneeDiplomeID" });
            DropColumn("dbo.AnneeUnivs", "AnneeDiplomeID");
            DropTable("dbo.Diplomes");
        }
    }
}
