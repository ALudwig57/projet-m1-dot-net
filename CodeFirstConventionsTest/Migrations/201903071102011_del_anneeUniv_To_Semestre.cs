namespace CodeFirstConventionsTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class del_anneeUniv_To_Semestre : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Semestres", "AnneeUniv_ID", "dbo.AnneeUnivs");
            DropIndex("dbo.Semestres", new[] { "AnneeUniv_ID" });
            DropColumn("dbo.Semestres", "AnneeUniv_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Semestres", "AnneeUniv_ID", c => c.Int());
            CreateIndex("dbo.Semestres", "AnneeUniv_ID");
            AddForeignKey("dbo.Semestres", "AnneeUniv_ID", "dbo.AnneeUnivs", "ID");
        }
    }
}
