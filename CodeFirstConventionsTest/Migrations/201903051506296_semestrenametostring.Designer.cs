// <auto-generated />
namespace CodeFirstConventionsTest.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class semestrenametostring : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(semestrenametostring));
        
        string IMigrationMetadata.Id
        {
            get { return "201903051506296_semestrenametostring"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
