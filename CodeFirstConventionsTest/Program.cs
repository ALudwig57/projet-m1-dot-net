﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeFirstConventionsTest
{
    class Program
    {
        static void Main(string[] args)
        {

            using (MyContext db = new MyContext())
            {
                for (int i = 1; i < 4; i++)
                {
                    var S1 = new Semestre();
                    var S2 = new Semestre();
                    var annee = new AnneeUniv();
                    annee.year = "L"+i.ToString();

                    S1.name = "S1 de L"+i.ToString();
                    S2.name = "S2 de L"+i.ToString();

                    annee.S1 = S1;
                    annee.S2 = S2;

                    db.AnneeUnivs.Add(annee);
                    db.SaveChanges();
                }
            }

            using (MyContext db = new MyContext())
            {
                var diplome = new Diplome();
                diplome.libelle = "Licence";
                foreach (AnneeUniv anneeUniv in db.AnneeUnivs.ToList())
                {
                    diplome.AnneeUnivs.Add(anneeUniv);
                }
                db.Diplomes.Add(diplome);
                db.SaveChanges();
            }

            using (MyContext db = new MyContext())
            {
                Diplome d = db.Diplomes.First<Diplome>();
                AnneeUniv anneeUniv = db.AnneeUnivs.First<AnneeUniv>();
                Console.WriteLine(d.libelle);
                Console.WriteLine(d.AnneeUnivs.First<AnneeUniv>().year);
                Console.WriteLine(d.AnneeUnivs.First<AnneeUniv>().S1.name);
                Console.WriteLine(anneeUniv.Diplome.libelle);
                Console.ReadLine();

            }

        }
    }

    public class AnneeUniv
    {
        public int ID { get; set; }
        public string year { get; set; }

        public int S1ID { get; set; }
        public int S2ID { get; set; }
        [ForeignKey("S1ID")]
        public virtual Semestre S1 { get; set; }
        [ForeignKey("S2ID")]
        public virtual Semestre S2 { get; set; }

        public int? AnneeDiplomeID { get; set; }
        [ForeignKey("AnneeDiplomeID")]
        public virtual Diplome Diplome { get; set; }
    }

    public class Semestre
    {
        public int ID { get; set; }
        public string name { get; set; }
    }

    public class Diplome
    {
        public Diplome()
        {
            AnneeUnivs = new List<AnneeUniv>();
        }
        public int ID { get; set; }
        public string libelle { get; set; }
        [ForeignKey("AnneeDiplomeID")]
        public virtual ICollection<AnneeUniv> AnneeUnivs { get; set; }
    }

    public class MyContext : DbContext
    {
        public MyContext() : base() { }
        public DbSet<AnneeUniv> AnneeUnivs { get; set; }
        public DbSet<Semestre> Semestres { get; set; }
        public DbSet<Diplome> Diplomes { get; set; }
    }

}
