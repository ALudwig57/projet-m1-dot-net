﻿namespace Projet_dot_net_2019
{
    public class ItemWithID
    {
        private int _hiddenId;
        private string _element;

        public ItemWithID(int id, string element)
        {
            _hiddenId = id;
            _element = element;
        }

        public int HiddenID
        {
            get { return _hiddenId; }
        }

        public override string ToString()
        {
            return _element;
        }
    }
}
