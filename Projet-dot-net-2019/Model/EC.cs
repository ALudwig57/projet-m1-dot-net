﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table EC
    /// </summary>
    public class EC
    {
        /// <summary>
        /// Constructeur d'EC : Initialisation de la liste
        /// de personnels et la liste de séance
        /// </summary>
        public EC()
        {
            this.Personnels = new List<Personnel>();
            this.Seances = new List<Seance>();
        }
        //Clé primaire de l'EC
        public int ID { get; set; }
        //Nom de l'EC
        public string Nom { get; set; }
        //Clé étrangère UE
        public int? UE_ECID { get; set; }
        [ForeignKey("UE_ECID")]
        //UE 
        public virtual UE UE { get; set; }
        [ForeignKey("SeanceECID")]
        //Liste de séances
        public virtual ICollection<Seance> Seances { get; set; }
        //Liste de personnels
        public virtual ICollection<Personnel> Personnels { get; set; }

        /// <summary>
        /// Cumul des heures des séances de l'EC
        /// </summary>
        public int CumulHeures
        {
            get
            {
                int cumul = 0;
                foreach (var seance in Seances)
                {
                    cumul += seance.NbHeures;
                }

                return cumul;
            }
        }
    }
}
