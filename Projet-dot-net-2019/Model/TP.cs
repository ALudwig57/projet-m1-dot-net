﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table TP
    /// </summary>
    public class TP : Travaux
    {
        public override string TypeCours => "TP";
    }
}
