﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table TD
    /// </summary>
    public class TD : Travaux
    {
        public override string TypeCours => "TD";
    }
}
