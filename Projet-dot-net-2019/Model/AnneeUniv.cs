﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle de l'année universitaire
    /// </summary>
    public class AnneeUniv
    {
        //ID unique de l'année universitaire (clef étrangère dans la table)
        public int ID { get; set; }
        //Nom de l'année
        public String year { get; set; }

        //Clé étrangère pour le premier semestre
        public int S1ID { get; set; }
        //Clé étrangère pour le deuxième semestre
        public int S2ID { get; set; }
        [ForeignKey("S1ID")]
        //Premier semestre de l'année
        public virtual Semestre S1 { get; set; }
        [ForeignKey("S2ID")]
        //Deuxième semestre de l'année
        public virtual Semestre S2 { get; set; }

        //Clé étrangère du diplôme
        public int? AnneeDiplomeID { get; set; }
        [ForeignKey("AnneeDiplomeID")]
        //Diplôme correspondant à l'année
        public virtual Diplome Diplome { get; set; }
    }
}
