﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table diplôme
    /// </summary>
    public class Diplome
    {
        //Constructeur Diplôme, initialisation de la liste
        //des années universitaires
        public Diplome()
        {
            AnneeUnivs = new List<AnneeUniv>();
        }
        //Id / Clé primaire
        public int ID { get; set; }
        //Nom du diplôme
        public string Nom { get; set; }
        [ForeignKey("AnneeDiplomeID")]
        //Liste des années composant le diplôme
        public virtual ICollection<AnneeUniv> AnneeUnivs { get; set; }
    }
}
