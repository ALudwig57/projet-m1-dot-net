﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table Seance
    /// </summary>
    public abstract class Seance
    {
        //Clé primaire de la séance
        public int ID { get; set; }
        //Nombre d'heures total de la séance
        public int NbHeures { get; set; }


        //Clé étrangère de EC
        public int? SeanceECID { get; set; }
        [ForeignKey("SeanceECID")]
        //EC correspondant à la séance
        public virtual EC EC { get; set; }


        //Clef étrangère de Personnel
        public int? PersonnelSeanceID { get; set; }
        [ForeignKey("PersonnelSeanceID")]
        //Personnel qui assure la séance
        public virtual Personnel Personnel { get; set; }

        [NotMapped]
        //Type de cours, non inscrit dans la table
        public abstract string TypeCours { get; }
    }
}
