﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table Semestre
    /// </summary>
    public class Semestre
    {
        /// <summary>
        /// Constructeur de Semestre : Initialisation de la liste des UE
        /// </summary>
        public Semestre()
        {
            UEs = new List<UE>();
        }
        //clef primaire
        public int ID { get; set; }
        //nom du semestre
        public String name { get; set; }
        [ForeignKey("SemestreUEID")]
        //Liste des Ue composant le semestre
        public virtual ICollection<UE> UEs { get; set; }

        //Inutile
        public void addUE(UE ue)
        {
            this.UEs.Add(ue);
        }
    }
} 