﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table UE
    /// </summary>
    public class UE
    {
        //Clé primaire
        public int ID { get; set; }
        //Nom de l'UE
        public string Nom { get; set; }
        //Clé étrangère de semestre
        public int SemestreUEID { get; set; }
        [ForeignKey("SemestreUEID")]
        //Semestre qui contient l'UE
        public virtual Semestre Semestre { get; set; }
        [ForeignKey("UE_ECID")]
        //Liste des EC de l'UE
        public virtual ICollection<EC> ECs { get; set; }

        /// <summary>
        /// Constructeur de l'UE : Initialise la liste des EC
        /// </summary>
        public UE()
        {
            ECs = new List<EC>();
        }

        /// <summary>
        /// Cumul des heures de l'UE => Somme des cumuls d'heures de toutes ses ECs
        /// </summary>
        public int CumulHeures
        {
            get
            {
                int cumul = 0;
                foreach (var ec in ECs)
                {
                    cumul += ec.CumulHeures;
                }

                return cumul;
            }
        }

        /// <summary>
        /// Liste des enseignants qui assurent des séances de cet UE
        /// </summary>
        public IEnumerable<Personnel> Profs
        {
            get
            {
                IEnumerable<Personnel> profs = new List<Personnel>();
                foreach (var ec in ECs)
                {
                    profs = profs.Concat(ec.Personnels);
                }

                return profs;
            }
        }
    }
}
