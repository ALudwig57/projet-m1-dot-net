﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Les travaux sont des cours avec des groupes (TD, TP, EI)
    /// </summary>
    public abstract class Travaux : Seance
    {
        //Nombre de groupes du cours
        public int NbGroupes { get; set; }
    }
}
