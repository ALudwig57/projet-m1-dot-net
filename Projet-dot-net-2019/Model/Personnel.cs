﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle / Table Personnel
    /// </summary>
    public class Personnel
    {
        //Clé primaire du personnel
        public int Id { get; set; }
        //Constructeur de Personnel : Initialisation de la liste des séances
        public Personnel()
        {
            this.Seances = new List<Seance>();
        }
        //Clé étrangère Categorie
        public int CategoriePersonnelID { get; set; }
        [ForeignKey("CategoriePersonnelID")]
        //Catégorie du personnel
        public Categorie Categorie { get; set; }
        //Nom du personnel
        public string Nom { get; set; }
        //Prénom
        public string Prenom { get; set; }
        //Email du personnel
        public string Email { get; set; }
        //Inutile
        public int NbHeures { get; set; }
        [ForeignKey("PersonnelSeanceID")]
        //Liste des séances gérées par le personnel
        public virtual ICollection<Seance> Seances { get; set; }

        /// <summary>
        /// Le cumul des heures des séances assurées par le personnel. Donnée non incluse dans la table
        /// </summary>
        [NotMapped]
        public double NbHeuresEffectives
        {
            get
            {
                double h = 0;
                foreach (var seance in Seances)
                {
                    //Si le cours est un cm
                    if (seance.TypeCours == "CM")
                        h += seance.NbHeures * OutilsVues.calculEq(Categorie.EqCM);
                    //Si le cours est un td
                    else if (seance.TypeCours == "TD")
                        h += seance.NbHeures;
                    //Si le cours est un TP
                    else if (seance.TypeCours == "TP")
                        h += seance.NbHeures * OutilsVues.calculEq(Categorie.EqTP);
                    //Si le cours est une EI
                    else if (seance.TypeCours == "EI")
                        h += seance.NbHeures * OutilsVues.calculEq(Categorie.EqEI);
                }

                return h;
            }
        }
    }
}
