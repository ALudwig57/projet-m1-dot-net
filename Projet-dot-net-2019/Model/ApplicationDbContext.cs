﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Contexte entity framework, contenant tous les ORM
    /// du programme.
    /// On utilisera les listes ci dessous pour accéder aux données
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base() { }
        public DbSet<Personnel> personnel { get; set; }
        public DbSet<CM> cm { get; set; }
        public DbSet<TD> td { get; set; }
        public DbSet<TP> tp { get; set; }
        public DbSet<EI> ei { get; set; }
        public DbSet<UE> ue { get; set; }
        public DbSet<EC> ec { get; set; }
        public DbSet<Seance> seance { get; set; }
        public DbSet<AnneeUniv> anneeUniv { get; set; }
        public DbSet<Semestre> semestre { get; set; }
        public DbSet<Categorie> categorie { get; set; }
        public DbSet<Diplome> diplomes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }

}
