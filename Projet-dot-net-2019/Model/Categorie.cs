﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Projet_dot_net_2019.Model
{
    /// <summary>
    /// Modèle/table Categorie
    /// </summary>
    public class Categorie
    {
        /// <summary>
        /// Constructeur, initialisation de la liste de personnels
        /// </summary>
        public Categorie()
        {
            Personnels = new List<Personnel>();
        }
        //ID de la catégorie / clef étrangère
        public int ID { get; set; }
        //Nom de la catégorie
        public string Nom { get; set; }
        //Nombre d'heures qu'un personnel
        //appartenent à la catégorie doit faire
        public int NbHeures { get; set; }
        //Equivalence CM
        public string EqCM { get; set; }
        //Equivalence TP
        public string EqTP { get; set; }
        //Equivalence EI
        public string EqEI { get; set; }
        [ForeignKey("CategoriePersonnelID")]
        //Liste des personnels appartenent à la catégorie
        public virtual ICollection<Personnel> Personnels { get; set; }

        //Surcharge d'opérateur ==
        public static bool operator==(Categorie a, Categorie b)
        {
            return a != null && (b != null && a.ID == b.ID);
        }

        //Surcharge d'opérateur !=
        public static bool operator !=(Categorie a, Categorie b)
        {
            return a != null && (b != null && a.ID != b.ID);
        }
    }
}
