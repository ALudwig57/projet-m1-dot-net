﻿using System.Windows.Forms;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle Diplome (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class DiplomeController: Controller<Diplome>, IController
    {
        private AnneeUnivService anneeUnivService;
        public DiplomeController(DiplomeService service, AnneeUnivService anneeUnivService) : base(service)
        {
            this.anneeUnivService = anneeUnivService;
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override Form CreateForm()
        {
            return new FormAddDiplome((DiplomeService)service, anneeUnivService);
        }

        public override UserControl CreateFiche(int id)
        {
            return new FicheDiplome(id, (DiplomeService)service);
        }

        public override Form EditForm(int id)
        {
            return new FormAddDiplome(id, (DiplomeService)service, anneeUnivService);
        }
    }
}