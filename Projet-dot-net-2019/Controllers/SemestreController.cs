﻿using System;
using System.Windows.Forms;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle Semestre (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class SemestreController: Controller<Semestre>, IController
    {
        private UEService ueService;
        private AnneeUnivService anneeUnivService;
        private PersonnelService personnelService;

        public SemestreController(SemestreService service, UEService ueService, AnneeUnivService anneeUnivService, PersonnelService personnelService) : base(service)
        {
            this.ueService = ueService;
            this.anneeUnivService = anneeUnivService;
            this.personnelService = personnelService;
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override Form CreateForm()
        {
            throw new NotImplementedException();
        }

        public override UserControl CreateFiche(int id)
        {
            return new FicheSemestre(id, (SemestreService)service);
        }

        public override Form EditForm(int id)
        {
            throw new NotImplementedException();
            //return new FormAddSemestre(id, (SemestreService)service, ueService, anneeUnivService, personnelService);
        }
    }
}