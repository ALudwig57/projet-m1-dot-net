﻿using System.Windows.Forms;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Interface permettant de déclarer les fonctions utiles pour la partie vue
    /// </summary>
    public interface AffichageController
    {
        /// <summary>
        /// FillTree permet de remplir des treeview avec des informations de façon ordonnée (les liens entre les noeuds se font via les 
        /// clés étrangères / primaires
        /// </summary>
        /// <param name="treeView">TreeView à remplir</param>
        void FillTree(TreeView treeView);
    }
}