﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Controller de la gestion du personnel
    /// </summary>
    public class GestionPersonnelController : AffichageController
    {
        private CategorieService categorieService;
        private PersonnelService personnelService;

        /// <summary>
        /// Constructeur du contrôleur
        /// </summary>
        /// <param name="categorieService">Service du modèle/de la table Categorie </param>
        /// <param name="personnelService">Service du modèle/de la table Personnel</param>
        public GestionPersonnelController(CategorieService categorieService, PersonnelService personnelService)
        {
            this.categorieService = categorieService;
            this.personnelService = personnelService;
        }

        /// <summary>
        /// Similaire que la fonction FillTree de GestionCoursController mais cette fois çi avec Categorie->Personnel
        /// </summary>
        /// <param name="treeView"></param>
        public void FillTree(TreeView treeView)
        {
            treeView.Nodes.Clear();
            foreach (Categorie categorie in categorieService.GetAllCategories())
            {
                OutilsVues.AjoutNoeudRacine(treeView.Nodes, categorie.Nom, "Categorie_"+categorie.ID.ToString());

                List<Personnel> listePersonnels = personnelService.GetPersonnelsFromCategory(categorie).ToList();
                Console.WriteLine("");
                foreach (Personnel personnel in listePersonnels)
                {
                    string intitulePersonnel = personnel.Nom + " " + personnel.Prenom + " " + personnel.NbHeuresEffectives;
                    if (personnel.NbHeuresEffectives > categorie.NbHeures)
                    {
                        OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Categorie_" + categorie.ID.ToString(), intitulePersonnel, "Personnel_" + personnel.Id.ToString(), false, System.Drawing.Color.Red);
                    }
                    else if (personnel.NbHeuresEffectives < categorie.NbHeures)
                    {
                        OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Categorie_" + categorie.ID.ToString(), intitulePersonnel, "Personnel_" + personnel.Id.ToString(), false, System.Drawing.Color.Green);
                    }
                    else
                    {
                        OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Categorie_" + categorie.ID.ToString(), intitulePersonnel, "Personnel_" + personnel.Id.ToString(), false, System.Drawing.Color.Gold);
                    }
                }
            }
        }
    }
}