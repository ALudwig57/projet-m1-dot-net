﻿using System.Linq;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur associé à la gestion de cours : Contient tous les services associés aux tables/modèles concernés
    /// </summary>
    public class GestionCoursController : AffichageController
    {
        //Services utiles pour la gestion de cours
        private DiplomeService diplomeService;
        private AnneeUnivService anneeUnivService;
        private SemestreService semestreService;
        private UEService ueService;
        private ECService ecService;

        /// <summary>
        /// Constructeur du contrôleur
        /// </summary>
        /// <param name="diplomeService">  Service de la table/du modèle Diplôme</param>
        /// <param name="anneeUnivService">Service de la table/du modèle AnneeUniv</param>
        /// <param name="semestreService"> Service de la table/du modèle Semestre</param>
        /// <param name="ueService">       Service de la table/du modèle UE</param>
        /// <param name="ecService">       Service de la table/du modèle EC</param>
        public GestionCoursController(DiplomeService diplomeService, AnneeUnivService anneeUnivService, SemestreService semestreService, UEService ueService, ECService ecService)
        {
            this.diplomeService = diplomeService;
            this.anneeUnivService = anneeUnivService;
            this.semestreService = semestreService;
            this.ueService = ueService;
            this.ecService = ecService;
        }

        /// <summary>
        ///    Remplie/Mets à jour le TreeView de la fenêtre de gestion de cours de cette façon:
        ///    Diplome -> AnneeUniv -> Semestre -> UE -> EC -> Seance
        /// </summary>
        /// <param name="treeView">TreeView à remplir</param>
        public void FillTree(TreeView treeView)
        {
            //On efface les noeuds
            treeView.Nodes.Clear();

            //Pour chaque diplôme dans la table Diplome
            foreach (Diplome diplome in diplomeService.GetAllDiplomes())
            {
                //On ajoute une noeud racine avec le nom du diplôme comme libellé, et comme tag Diplome_ + sa clé primaire
                OutilsVues.AjoutNoeudRacine(treeView.Nodes, diplome.Nom, "Diplome_"+diplome.ID.ToString());

                //Pour chaque année universitaire dans la table AnneeUniv
                foreach (AnneeUniv anneeUniv in anneeUnivService.GetAnneeUnivsFromDiplome(diplome.ID).ToList())
                {
                    
                    //On ajoute un noeud enfant avec le nom de l'année comme libellé, et comme tag Annee_ + sa clé primaire
                    OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Diplome_"+diplome.ID.ToString(), anneeUniv.year, "Annee_"+anneeUniv.ID.ToString(), false);
                    //On ajoute un noeud enfant au noeud de l'année avec ses semestres
                    OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Annee_"+anneeUniv.ID.ToString(), "Semestre 1", "Semestre_"+anneeUniv.S1.ID.ToString(), true);
                    OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Annee_"+anneeUniv.ID.ToString(), "Semestre 2", "Semestre_"+anneeUniv.S2.ID.ToString(), true);

                    //Pour chaque UE dans la table UE qui appartient au premier semestre
                    foreach (UE ue in semestreService.GetUESOfSemestre(anneeUniv.S1.ID).ToList())
                    {
                        //On ajoute un noeud correspondant à l'UE en dessous de son semestre
                        OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Semestre_"+anneeUniv.S1.ID.ToString(), ue.Nom, "UE_"+ue.ID.ToString(), true);

                        //Pour chaque EC dans la table EC qui appartient à l'UE ajoutée précédemment
                        foreach (EC ec in ueService.GetECs(ue.ID))
                        {

                            //On ajoute un noeud correspondant à l'EC en dessous de son UE
                            OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "UE_"+ue.ID.ToString(), ec.Nom, "EC_"+ec.ID.ToString(), true);
                            //Pour chaque séanee dans la table Seance qui appartient à l'EC ajoutée précédemment
                            foreach (Seance seance in ecService.GetSeances(ec.ID))
                            {
                                //On ajoute un noeud correspondant à la séance en dessous de son EC
                                OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "EC_"+ec.ID.ToString(), seance.TypeCours, "Seance_"+seance.ID.ToString(), true);
                            }
                        }
                    }

                    //Pour chaque UE dans la table UE qui appartient au second semestre
                    foreach (UE ue in semestreService.GetUESOfSemestre(anneeUniv.S2.ID).ToList())
                    {

                        //On ajout le noeud de l'Ue en dessous de son semestre
                        OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "Semestre_"+anneeUniv.S2.ID.ToString(), ue.Nom, "UE_"+ue.ID.ToString(), true);

                        //Pour chaque EC de l'UE
                        foreach (EC ec in ueService.GetECs(ue.ID))
                        {
                            //On ajoute un noeud correspondant à l'EC
                            OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "UE_"+ue.ID.ToString(), ec.Nom, "EC_"+ec.ID.ToString(), true);
                            //Pour chaque Seance de l'EC
                            foreach (Seance seance in ecService.GetSeances(ec.ID))
                            {
                                //On ajoute un noeud correspondant à la séance
                                OutilsVues.AjoutNoeudEnfant(treeView.Nodes, "EC_"+ec.ID.ToString(), seance.TypeCours, "Seance_"+seance.ID.ToString(), true);
                            }
                        }
                    }
                }
            }
        }
    }
}