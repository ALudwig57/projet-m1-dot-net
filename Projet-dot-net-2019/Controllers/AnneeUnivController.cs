﻿using System.Windows.Forms;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle AnneeUniv (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class AnneeUnivController: Controller<AnneeUniv>, IController
    {
        private DiplomeService diplomeService;

        public AnneeUnivController(AnneeUnivService service, DiplomeService diplomeService) : base(service)
        {
            this.diplomeService = diplomeService;
        }

        public override Form CreateForm()
        {
            return new FormAddAnnee((AnneeUnivService)service);
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override UserControl CreateFiche(int id)
        {
            return new FicheAnneUniv(id, (AnneeUnivService)service, diplomeService);
        }

        public override Form EditForm(int id)
        {
            return new FormAddAnnee(id, (AnneeUnivService)service);
        }
    }
}