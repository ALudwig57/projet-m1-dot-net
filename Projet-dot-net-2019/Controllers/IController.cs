﻿using System.Windows.Forms;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Interface contenant les différentes méthodes des contrôleurs
    /// </summary>
    public interface IController
    {
        UserControl CreateFiche(int id);

        Form CreateForm();

        Form EditForm(int id);

        void delete(int id);
    }
}