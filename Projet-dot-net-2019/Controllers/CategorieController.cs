﻿using System.Windows.Forms;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle Categorie (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class CategorieController: Controller<Categorie>, IController
    {
        public CategorieController(CategorieService service) : base(service)
        {
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override Form CreateForm()
        {
            return new FormAddCategorie((CategorieService) service);
        }

        public override UserControl CreateFiche(int id)
        {
            return new FicheCategory(id, (CategorieService)service);
        }

        public override Form EditForm(int id)
        {
            return new FormAddCategorie(id, (CategorieService)service);
        }
    }
}