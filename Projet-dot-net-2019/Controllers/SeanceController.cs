﻿using System.Windows.Forms;
using Projet_dot_net_2019.Fiches;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle Seance (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class SeanceController: Controller<Seance>, IController
    {
        private ECService ecService;
        private PersonnelService personnelService;

        public SeanceController(SeanceService service, ECService ecService, PersonnelService personnelService) : base(service)
        {
            this.ecService = ecService;
            this.personnelService = personnelService;
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override Form CreateForm()
        {
            return new FormAddSeance((SeanceService)service, ecService, personnelService);
        }

        public override UserControl CreateFiche(int id)
        {
            return new FicheSeance(id, (SeanceService)service, ecService);
        }

        public override Form EditForm(int id)
        {
            return new FormAddSeance(id, (SeanceService)service, ecService, personnelService);
        }
    }
}