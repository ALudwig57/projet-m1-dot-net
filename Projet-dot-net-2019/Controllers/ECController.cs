﻿using System;
using System.Windows.Forms;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Fiches;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle EC (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class ECController: Controller<EC>, IController
    {
        private UEService ueService;
        private SeanceService seanceService;

        public ECController(ECService service, UEService ueService, SeanceService seanceService) : base(service)
        {
            this.seanceService = seanceService;
            this.ueService = ueService;
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override Form CreateForm()
        {
            return new FormAddEC((ECService)service, ueService, seanceService);
        }

        public override UserControl CreateFiche(int id)
        {
            return new FicheEC(id, (ECService)service, ueService, seanceService);
        }

        public override Form EditForm(int id)
        {
            return new FormAddEC(id, (ECService)service, ueService, seanceService);
        }
    }
}