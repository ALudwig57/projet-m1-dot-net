﻿using System.Windows.Forms;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle UE (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class UEController: Controller<UE>, IController
    {
        private SemestreService semestreService;
        private AnneeUnivService anneeUnivService;
        private ECService ecService;
        private SeanceService seanceService;

        public UEController(UEService service, SemestreService semestreService, AnneeUnivService anneeUnivService, ECService ecService, SeanceService seanceService) : base(service)
        {
            this.semestreService = semestreService;
            this.anneeUnivService = anneeUnivService;
            this.ecService = ecService;
            this.seanceService = seanceService;
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override Form CreateForm()
        {
            return new FormAddUE((UEService)service, ecService, semestreService, seanceService);
        }

        public override UserControl CreateFiche(int id)
        {
            return new FicheUE(id, (UEService)service, anneeUnivService);
        }

        public override Form EditForm(int id)
        {
            return new FormAddUE(id, (UEService)service, ecService, semestreService, seanceService);
        }
    }
}