﻿using System.Windows.Forms;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur de la table/ du modèle Personnel (pour la doc des méthodes, voir la doc dans Controller.cs)
    /// </summary>
    public class PersonnelController: Controller<Personnel>, IController
    {
        private CategorieService categorieService;

        public PersonnelController(PersonnelService service, CategorieService categorieService) : base(service)
        {
            this.categorieService = categorieService;
        }

        public override void delete(int id)
        {
            this.service.Delete(id);
        }

        public override Form CreateForm()
        {
            return new FormAddPersonnel((PersonnelService) service, categorieService);
        }

        public override UserControl CreateFiche(int id)
        {
            return new FichePersonnel(id, (PersonnelService)service);
        }

        public override Form EditForm(int id)
        {
            return new FormAddPersonnel(id, (PersonnelService)service, categorieService);
        }
    }
}