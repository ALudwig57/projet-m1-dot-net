﻿using System.Windows.Forms;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.AbstractFactories.Controllers
{
    /// <summary>
    /// Contrôleur pour les différents modèles/tables. Il permettra de pouvoir générer des formulaires graphiques individuels en fonction de l'id 
    /// du modèle.
    /// </summary>
    /// <typeparam name="TEntity">Çlasse liée à la table / au modèle</typeparam>
    public abstract class Controller<TEntity> where TEntity : class
    {
        /// <summary>
        /// Service correspondant au modèle de TEntity
        /// </summary>
        public Service<TEntity> service { get; set; }

        /// <summary>
        /// Constructeur du controller
        /// </summary>
        /// <param name="service">Service correspondant au modèle de TEntity</param>
        public Controller(Service<TEntity> service)
        {
            this.service = service;
        }

        /// <summary>
        /// Méthode permettrant de créer une fiche d'information (UserControl) individuelle avec les informations de l'entrée 
        /// de la base
        /// </summary>
        /// <param name="id">id de l'objet que l'on veut afficher</param>
        /// <returns>Le UserControl correspondant à l'entrée aue l'on veut afficher</returns>
        public abstract UserControl CreateFiche(int id);

        /// <summary>
        /// Méthode de création de formulaire d'ajout d'objet, avec les différents champs à remplir
        /// </summary>
        /// <returns>Formulaire d'ajout</returns>
        public abstract Form CreateForm();

        /// <summary>
        /// Méthode de création de formulaire d'édition. Très similaire à CreateForm. 
        /// </summary>
        /// <param name="id">id de d'objet que l'on souhaite modifier</param>
        /// <returns>Formulaire de modification</returns>
        public abstract Form EditForm(int id);

        /// <summary>
        /// méthode de suppression d'objet
        /// </summary>
        /// <param name="id">id de l'objet à supprimer</param>
        public abstract void delete(int id);
    }
}