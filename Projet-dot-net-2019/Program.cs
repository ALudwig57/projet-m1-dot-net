﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;
using System.Data.Entity;
using Projet_dot_net_2019.AbstractFactories.Controllers;
using Projet_dot_net_2019.Repositories;

namespace Projet_dot_net_2019
{
    /// <summary>
    /// Programme principal (main)
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// Le contexte, les services et les controleurs y sont initialisés, puis l'application est lancée
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Contexte initialisé
            ApplicationDbContext context = new ApplicationDbContext();
        
            //Services initialisé avec une nouvelle instance de repository respectif afin de pouvoir accéder aux méthodes CRUD
            DiplomeService diplomeService = new DiplomeService(new DiplomeRepository(context));
            SemestreService semestreService = new SemestreService(new SemestreRepository(context));
            AnneeUnivService anneeUnivService = new AnneeUnivService(new AnneeUnivRepository(context))
            {
                SemestreService = semestreService
            };
            diplomeService.AnneeUnivService = anneeUnivService;
            UEService ueService = new UEService(new UERepository(context))
            {
                SemestreService = semestreService
            };
            semestreService.UEService = ueService;
            ECService ecService = new ECService(new ECRepository(context))
            {
                UEService = ueService
            };
            ueService.ECService = ecService;
            SeanceService seanceService = new SeanceService(new SeanceRepository(context));
            ecService.SeanceService = seanceService;

            CategorieService categorieService = new CategorieService(new CategorieRepository(context));
            PersonnelService personnelService = new PersonnelService(new PersonnelRepository(context));
            personnelService.CategorieService = categorieService;
            personnelService.SeanceService = seanceService;
            categorieService.PersonnelService = personnelService;
            //categorieService.PersonnelService = personnelService;

            //initialisation des contrôleurs du modèle, puis stockés soit dans une liste pour la partie personnel,
            //soit dans une liste pour la partie cours

            // Initialisation des controleurs de la partie personnel
            List<IController> listeControllersPersonnel = new List<IController>
            {
                new CategorieController(categorieService),
                new PersonnelController(personnelService, categorieService)
            };

            // Initialisation des controleurs de la partie cours
            List<IController> listeControllersCours = new List<IController>
            {
                new DiplomeController(diplomeService, anneeUnivService),
                new AnneeUnivController(anneeUnivService, diplomeService),
                new SemestreController(semestreService, ueService, anneeUnivService, personnelService),
                new UEController(ueService, semestreService, anneeUnivService, ecService, seanceService),
                new ECController(ecService, ueService, seanceService),
                new SeanceController(seanceService, ecService, personnelService)
            };

            //Initialisation du contr^leur de la gestion de cours, avec les services dédiés en paramètre
            GestionCoursController gestionCoursController = new GestionCoursController(diplomeService, anneeUnivService, semestreService, ueService, ecService);
            //Initialisation du contr^leur de la gestion de personnel, avec les services dédiés en paramètre
            GestionPersonnelController gestionPersonnelController = new GestionPersonnelController(categorieService, personnelService);

            //Par défaut pour windows forms
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Lancement de l'interface graphique
            Application.Run(new GestionUFRMIM(listeControllersPersonnel, listeControllersCours, gestionPersonnelController, gestionCoursController));
        }
    }
}
