﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Vues.CORAddSeance
{
    //Ajout de TD
    class TdAdder : Adder
    {
        public override Seance ProcessRequest(int id, int nbHeures, Personnel personnel, EC ec, int nbGroupe)
        {
            if (id == 1)
            {
                if (personnel.NbHeuresEffectives + nbHeures  > personnel.Categorie.NbHeures)
                {
                    if (!alertHours())
                    {
                        return null;
                    }
                }
                Seance seance = new TD();
                ((TD)seance).NbHeures = nbHeures;
                ((TD)seance).NbGroupes = nbGroupe;
                ((TD)seance).Personnel = personnel;
                seance.EC = ec;
                seance.EC.Personnels.Add(seance.Personnel);
                return seance;
            }
            else if (Supervisor != null)
            {
                return Supervisor.ProcessRequest(id, nbHeures, personnel, ec, nbGroupe);
            }
            
           
                return null;
            
        }
    }
}
