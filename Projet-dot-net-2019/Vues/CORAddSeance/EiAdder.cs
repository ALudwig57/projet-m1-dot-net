﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Vues.CORAddSeance
{
    /// <summary>
    /// Ajout d'EI
    /// </summary>
    class EiAdder : Adder
    {
        public override Seance ProcessRequest(int id, int nbHeures, Personnel personnel, EC ec, int nbGroupe)
        {
            if (id == 3)
            {
                //Si dépassement des heures effectives
                if (personnel.NbHeuresEffectives + nbHeures * OutilsVues.calculEq(personnel.Categorie.EqEI) > personnel.Categorie.NbHeures)
                {
                    //Si 'utilisateur refuse d'ajouter
                    if (!alertHours())
                    {
                        return null;
                    }
                }
                ///Ajout de l'EI
                Seance seance = new EI();
                ((EI)seance).NbHeures =nbHeures;
                ((EI)seance).NbGroupes = nbGroupe;
                ((EI)seance).Personnel = personnel;
                seance.EC = ec;
                seance.EC.Personnels.Add(seance.Personnel);
                return seance;
            }
            else if (Supervisor != null)
            {
                return Supervisor.ProcessRequest(id, nbHeures, personnel, ec, nbGroupe);
            }
            
            
                return null;
            
        }
    }
}
