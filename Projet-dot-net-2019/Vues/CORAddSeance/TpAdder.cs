﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Vues.CORAddSeance
{
    /// <summary>
    /// Ajout de TP
    /// </summary>
    class TpAdder : Adder
    {
        public override Seance ProcessRequest(int id, int nbHeures, Personnel personnel, EC ec, int nbGroupe)
        {
            if (id == 2)
            {
                if (personnel.NbHeuresEffectives + nbHeures * OutilsVues.calculEq(personnel.Categorie.EqTP) > personnel.Categorie.NbHeures)
                {
                    if (!alertHours())
                    {
                        return null;
                    }
                }

                Seance seance = new TP();
                ((TP)seance).NbHeures = nbHeures;
                ((TP)seance).NbGroupes = nbGroupe;
                ((TP)seance).Personnel = personnel;
                seance.EC = ec;
                seance.EC.Personnels.Add(seance.Personnel);

                return seance;
            }
            else if (Supervisor != null)
            {
                return Supervisor.ProcessRequest(id, nbHeures, personnel, ec, nbGroupe);
            }
            
            
                return null;
            
        }
    }
}
