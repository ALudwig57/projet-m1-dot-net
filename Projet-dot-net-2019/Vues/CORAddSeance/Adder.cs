﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Vues.CORAddSeance
{
    /// <summary>
    /// Maillon de chaine de responsabilité, Pour permettre d'ajouter ou modifier des séances en fonction du type
    /// </summary>
    abstract class Adder
    {
        protected Adder Supervisor;

        public void SetSupervisor(Adder supervisor)
        {
            this.Supervisor = supervisor;
        }

        public abstract Seance ProcessRequest(int id, int nbHeures,Personnel personnel, EC ec,int nbGroupe);

        /// <summary>
        /// Message d'avertissement affiché en cas de dépassement d'heures effectives
        /// </summary>
        /// <returns></returns>
        public Boolean alertHours()
        {
            var confirmResult = MessageBox.Show("Le nombre d'heures de la categorie sera depasse, voulez vous quand meme affecter ce cours ? ", "Alert",MessageBoxButtons.YesNo);
            return confirmResult == DialogResult.Yes;
        }
    }
}
