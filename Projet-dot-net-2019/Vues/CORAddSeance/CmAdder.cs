﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Vues.CORAddSeance
{
    /// <summary>
    /// Ajout d'un CM
    /// </summary>
    class CmAdder : Adder
    {
        

        public override Seance ProcessRequest(int id, int nbHeures, Personnel personnel,EC ec, int nbGroupe)
        {
            if(id == 0)
            {
               
                //Si dépassement des heures de la catégorie
                if(personnel.NbHeuresEffectives + nbHeures * OutilsVues.calculEq(personnel.Categorie.EqCM) > personnel.Categorie.NbHeures)
                {
                    //Si on refuse l'ajout 
                    if (!alertHours())
                    {
                        return null;
                    }
                }
                Seance seance = new CM();
                ((CM)seance).NbHeures = nbHeures;
                ((CM)seance).Personnel = personnel;
                seance.EC = ec;
                seance.EC.Personnels.Add(seance.Personnel);

                return seance;
            }
            else if(Supervisor!=null)
            {
               return Supervisor.ProcessRequest( id,  nbHeures,  personnel,ec,  nbGroupe);
            }
            
                return null;
            
            
        }
    }
}
