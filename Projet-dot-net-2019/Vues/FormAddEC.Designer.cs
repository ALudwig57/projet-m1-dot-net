﻿namespace Projet_dot_net_2019.Vues
{
    partial class FormAddEC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.b_cancel = new System.Windows.Forms.Button();
            this.b_valider = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(112, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 13);
            this.label3.TabIndex = 36;
            this.label3.Text = "Ajout d\'une nouvelle EC";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 120);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Nom";
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(137, 117);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(219, 20);
            this.tb_name.TabIndex = 34;
            // 
            // b_cancel
            // 
            this.b_cancel.Location = new System.Drawing.Point(37, 193);
            this.b_cancel.Name = "b_cancel";
            this.b_cancel.Size = new System.Drawing.Size(80, 21);
            this.b_cancel.TabIndex = 43;
            this.b_cancel.Text = "Annuler";
            this.b_cancel.UseVisualStyleBackColor = true;
            this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
            // 
            // b_valider
            // 
            this.b_valider.Location = new System.Drawing.Point(305, 193);
            this.b_valider.Name = "b_valider";
            this.b_valider.Size = new System.Drawing.Size(80, 21);
            this.b_valider.TabIndex = 42;
            this.b_valider.Text = "Valider";
            this.b_valider.UseVisualStyleBackColor = true;
            this.b_valider.Click += new System.EventHandler(this.b_valider_Click);
            // 
            // FormAddEC
            // 
            this.AcceptButton = this.b_valider;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 230);
            this.Controls.Add(this.b_cancel);
            this.Controls.Add(this.b_valider);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_name);
            this.Name = "FormAddEC";
            this.Text = "Ajouter un EC";
            this.Load += new System.EventHandler(this.FormAddEC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Button b_cancel;
        private System.Windows.Forms.Button b_valider;
    }
}