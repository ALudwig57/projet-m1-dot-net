﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Vues
{
    public partial class FormAddUE : Form
    {
        private int _currentId = -1;
        private UEService ueService;
        private ECService ecService;
        private SemestreService semestreService;
        private SeanceService seanceService;

        public FormAddUE(UEService ueService, ECService ecService, SemestreService semestreService, SeanceService seanceService)
        {
            this.ueService = ueService;
            this.ecService = ecService;
            this.semestreService = semestreService;
            this.seanceService = seanceService;
            InitializeComponent();
        }

        public FormAddUE(int id, UEService ueService, ECService ECService, SemestreService semestreService, SeanceService seanceService)
        {
            _currentId = id;
            this.ueService = ueService;
            this.ecService = ECService;
            this.semestreService = semestreService;
            this.seanceService = seanceService;
            UE ue = this.ueService.Get(_currentId);
            InitializeComponent();
            tb_name.Text = ue.Nom;
            b_valider.Text = "Modifier";
            label3.Text = "Modification d'une UE";
            this.Text = "Modifier une UE";
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form_add_ue_Load(object sender, EventArgs e)
        {
            FillCheckBoxList();
            FillCombobox();

        }

        private void FillCheckBoxList()
        {
            clb_ecs.Items.Clear();
            IEnumerable<EC> UEecs = null;
            IEnumerable<EC> ecsOprhelines = this.ecService.GetECOrphelines();
            UE ue = null;
            if (_currentId != -1)
            {
                ue = this.ueService.Get(_currentId);
                UEecs = ue.ECs;
                foreach (var ec in UEecs)
                {
                    clb_ecs.Items.Add(ec.Nom, true);
                }
            }
            foreach (var ec in ecsOprhelines)
            {
                clb_ecs.Items.Add(ec.Nom);
            }

        }

        private void FillCombobox()
        {
            comboBox_Semestre.Items.Clear();
            int i = 0;
            UE ue = null;
            if (_currentId != -1)
                ue = this.ueService.Get(_currentId);

            foreach (Semestre semestre in semestreService.GetAllSemestres())
            {
                comboBox_Semestre.Items.Add(new ItemWithID(semestre.ID, semestre.name));

                if (_currentId != -1)
                {
                    if (semestre.ID == ue.SemestreUEID)
                        comboBox_Semestre.SelectedItem = comboBox_Semestre.Items[i];
                }
                else
                {
                    comboBox_Semestre.SelectedIndex = 0;
                }

                i++;
            }
        }
        

        private List<EC> GetECListByChecked(CheckedListBox.CheckedItemCollection clc)
        {
            List<EC>  list = new List<EC>();
            foreach (string item in clc)
            {
                string libelle = item; //.ToString();
                list.Add(ecService.FindEC(x => x.Nom.Contains(libelle)).First());
            }
            return list;
        }


        private void b_valider_Click(object sender, EventArgs e)
        {
            if (_currentId != -1)
            {
                this.ueService.Update(_currentId, tb_name.Text, GetECListByChecked(clb_ecs.CheckedItems), ((ItemWithID)comboBox_Semestre.SelectedItem).HiddenID);
            }
            else
            {
                UE ue = new UE();
                ue.Nom = tb_name.Text;
                ue.SemestreUEID = ((ItemWithID) comboBox_Semestre.SelectedItem).HiddenID;
                ue.ECs = GetECListByChecked(clb_ecs.CheckedItems);
                ueService.Add(tb_name.Text, GetECListByChecked(clb_ecs.CheckedItems), ((ItemWithID)comboBox_Semestre.SelectedItem).HiddenID);
                //this.ueService.Add(tb_name.Text, GetECListByChecked(clb_ecs.CheckedItems), ((ItemWithID)comboBox_Semestre.SelectedItem).HiddenID);
            }
                
            this.Close();
        }

        private void b_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void addEC_Click(object sender, EventArgs e)
        {
            FormAddEC fa = new FormAddEC(ecService, ueService, seanceService);
            fa.ShowDialog(this);
            FillCombobox();
            FillCheckBoxList();
        }
    }
}
