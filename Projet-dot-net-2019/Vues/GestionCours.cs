﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Projet_dot_net_2019.AbstractFactories.Controllers;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Vues
{
    public partial class GestionCours : Form
    {
        private DiplomeController diplomeController;
        private AnneeUnivController anneeUnivController;
        private SemestreController semestreController;
        private UEController ueController;
        private ECController ecController;
        private SeanceController seanceController;

        private GestionCoursController gestionCoursController;

        private Form parent;

        public GestionCours(Form parent, List<IController> listeServicesCours, GestionCoursController gestionCoursController)
        {
            //0: Diplome, 1: Annee, 2: Semestre, 3: UE, 4: Seance
            diplomeController = (DiplomeController)listeServicesCours[0];
            anneeUnivController = (AnneeUnivController)listeServicesCours[1];
            semestreController = (SemestreController)listeServicesCours[2];
            ueController = (UEController)listeServicesCours[3];
            ecController = (ECController) listeServicesCours[4];
            seanceController = (SeanceController)listeServicesCours[5];

            this.gestionCoursController = gestionCoursController;

            this.parent = parent;
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }



        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            panel_fiche.Controls.Clear();
            int niveau = treeView1.SelectedNode.Level;
            int id = Int32.Parse(treeView1.SelectedNode.Name.Split('_')[1]);
            //UserControl fiche = NiveauController(niveau).CreateFiche(Int32.Parse(treeView1.SelectedNode.Name));
            UserControl fiche = NiveauController(niveau).CreateFiche(id);
            panel_fiche.Controls.Add(fiche);
            if (niveau == 2)
            {
                b_update.Enabled = false;
                b_delete.Enabled = false;
            }
            else
            {
                b_update.Enabled = true;
                b_delete.Enabled = true;
            }
            fiche.Dock = DockStyle.Fill;
            fiche.BringToFront();
        }

        private void Gestion_Cours_Load(object sender, EventArgs e)
        {
            //FillTree();
            gestionCoursController.FillTree(treeView1);
            treeView1.ExpandAll();
        }

        private void b_add_diplome_Click(object sender, EventArgs e)
        {
            FormAddDiplome formAddDiplome = (FormAddDiplome)diplomeController.CreateForm();
            formAddDiplome.ShowDialog(this);
            //FillTree();
            gestionCoursController.FillTree(treeView1);
        }

        private void b_add_annee_Click(object sender, EventArgs e)
        {
            FormAddAnnee fa = (FormAddAnnee) anneeUnivController.CreateForm();
            fa.ShowDialog(this);
            //FillTree();
            gestionCoursController.FillTree(treeView1);
        }

        private void b_add_ue_Click(object sender, EventArgs e)
        {
            FormAddUE fa = (FormAddUE)ueController.CreateForm();
            fa.ShowDialog(this);
            //FillTree();
            gestionCoursController.FillTree(treeView1);
        }

        private void b_add_ec_Click(object sender, EventArgs e)
        {
            FormAddEC fa = (FormAddEC)ecController.CreateForm();
            fa.ShowDialog(this);
            //FillTree();
            gestionCoursController.FillTree(treeView1);
        }

        private void b_add_cours_Click(object sender, EventArgs e)
        {
            FormAddSeance fa = (FormAddSeance)seanceController.CreateForm();
            fa.ShowDialog(this);
            //FillTree();
            gestionCoursController.FillTree(treeView1);
        }

        private void b_update_Click(object sender, EventArgs e)
        {
            int ID = Int32.Parse(treeView1.SelectedNode.Name.Split('_')[1]);
            int niveau = treeView1.SelectedNode.Level;
            Form formEdit = NiveauController(niveau).EditForm(ID);
            formEdit.ShowDialog(this);
            //FillTree();
            gestionCoursController.FillTree(treeView1);
        }


        private void b_delete_Click_1(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show( "Êtes vous certain de votre suppression ? Cette action est irréversible !", "Confirmer la suppression", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                int niveau = treeView1.SelectedNode.Level;
                int id = Int32.Parse(treeView1.SelectedNode.Name.Split('_')[1]);
                NiveauController(niveau).delete(id);
                //FillTree();
                gestionCoursController.FillTree(treeView1);
            }
        }

        private IController NiveauController(int niveau)
        {
            switch (niveau)
            {
                case 0:
                    return diplomeController;
                case 1:
                    return anneeUnivController;
                case 2:
                    return semestreController;
                case 3:
                    return ueController;
                case 4:
                    return ecController;
                case 5:
                    return seanceController;
                default:
                    throw new NotSupportedException();
            }
        }

        private void GestionCours_FormClosing(object sender, FormClosingEventArgs e)
        {
            parent.Show();
            this.Hide();
        }

        
    }
}
