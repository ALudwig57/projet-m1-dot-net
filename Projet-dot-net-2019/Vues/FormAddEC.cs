﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Vues
{
    public partial class FormAddEC : Form
    {
        private ECService ecService;
        private UEService ueService;
        private SeanceService seanceService;

        private int _currentId = -1;

        public FormAddEC(ECService ecService, UEService ueService, SeanceService seanceService)
        {
            this.ueService = ueService;
            this.ecService = ecService;
            this.seanceService = seanceService;
            InitializeComponent();
        }

        public FormAddEC(int id, ECService ecService, UEService ueService, SeanceService seanceService)
        {
            this._currentId = id;
            this.ueService = ueService;
            this.ecService = ecService;
            this.seanceService = seanceService;
            EC ec = this.ecService.Get(_currentId);
            InitializeComponent();
            tb_name.Text = ec.Nom;
            b_valider.Text = "Modifier";
            label3.Text = "Modification d'une EC";
            this.Text = "Modifier une EC";
        }

        private void FormAddEC_Load(object sender, EventArgs e)
        {
            
        }

        private void b_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void b_valider_Click(object sender, EventArgs e)
        {
            if (_currentId != -1)
            {
                EC ec = this.ecService.Get(_currentId);
                ec.Nom = tb_name.Text;
                this.ecService.Update(ec);
            }
            else
            {
                this.ecService.Add(tb_name.Text);
            }
            this.Close();
        }
    }
}
