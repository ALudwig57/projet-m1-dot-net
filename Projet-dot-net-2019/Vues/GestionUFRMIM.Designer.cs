﻿namespace Projet_dot_net_2019.Vues
{
    partial class GestionUFRMIM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gestionCours = new System.Windows.Forms.Button();
            this.gestionPersonnel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // gestionCours
            // 
            this.gestionCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestionCours.Location = new System.Drawing.Point(99, 80);
            this.gestionCours.Name = "gestionCours";
            this.gestionCours.Size = new System.Drawing.Size(276, 213);
            this.gestionCours.TabIndex = 0;
            this.gestionCours.Text = "Gestion des cours (diplômes, années, UE, ...)";
            this.gestionCours.UseVisualStyleBackColor = true;
            this.gestionCours.Click += new System.EventHandler(this.gestionCours_Click);
            // 
            // gestionPersonnel
            // 
            this.gestionPersonnel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestionPersonnel.Location = new System.Drawing.Point(427, 80);
            this.gestionPersonnel.Name = "gestionPersonnel";
            this.gestionPersonnel.Size = new System.Drawing.Size(276, 213);
            this.gestionPersonnel.TabIndex = 1;
            this.gestionPersonnel.Text = "Gestion du personnel (Catégories, ...)";
            this.gestionPersonnel.UseVisualStyleBackColor = true;
            this.gestionPersonnel.Click += new System.EventHandler(this.gestionPersonnel_Click);
            // 
            // GestionUFRMIM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.gestionPersonnel);
            this.Controls.Add(this.gestionCours);
            this.Name = "GestionUFRMIM";
            this.Text = "Gestion UFR MIM";
            this.Load += new System.EventHandler(this.GestionUFRMIM_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button gestionCours;
        private System.Windows.Forms.Button gestionPersonnel;
    }
}