﻿namespace Projet_dot_net_2019.Vues
{
    partial class FormAddSeance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_cancel = new System.Windows.Forms.Button();
            this.b_valider = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.nbHeures = new System.Windows.Forms.NumericUpDown();
            this.comboBoxProf = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.nbGroupes = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxEC = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nbHeures)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbGroupes)).BeginInit();
            this.SuspendLayout();
            // 
            // b_cancel
            // 
            this.b_cancel.Location = new System.Drawing.Point(30, 341);
            this.b_cancel.Name = "b_cancel";
            this.b_cancel.Size = new System.Drawing.Size(80, 21);
            this.b_cancel.TabIndex = 51;
            this.b_cancel.Text = "Annuler";
            this.b_cancel.UseVisualStyleBackColor = true;
            this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
            // 
            // b_valider
            // 
            this.b_valider.Location = new System.Drawing.Point(298, 341);
            this.b_valider.Name = "b_valider";
            this.b_valider.Size = new System.Drawing.Size(80, 21);
            this.b_valider.TabIndex = 50;
            this.b_valider.Text = "Valider";
            this.b_valider.UseVisualStyleBackColor = true;
            this.b_valider.Click += new System.EventHandler(this.b_valider_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(110, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(128, 13);
            this.label3.TabIndex = 46;
            this.label3.Text = "Ajout d\'un nouveau cours";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(39, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 45;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(39, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Nbheures";
            // 
            // comboBoxType
            // 
            this.comboBoxType.FormattingEnabled = true;
            this.comboBoxType.Location = new System.Drawing.Point(135, 106);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(219, 21);
            this.comboBoxType.TabIndex = 54;
            // 
            // nbHeures
            // 
            this.nbHeures.Location = new System.Drawing.Point(135, 146);
            this.nbHeures.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nbHeures.Name = "nbHeures";
            this.nbHeures.Size = new System.Drawing.Size(219, 20);
            this.nbHeures.TabIndex = 55;
            this.nbHeures.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // comboBoxProf
            // 
            this.comboBoxProf.FormattingEnabled = true;
            this.comboBoxProf.Location = new System.Drawing.Point(135, 220);
            this.comboBoxProf.Name = "comboBoxProf";
            this.comboBoxProf.Size = new System.Drawing.Size(219, 21);
            this.comboBoxProf.TabIndex = 57;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 223);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 56;
            this.label4.Text = "Professeur";
            // 
            // nbGroupes
            // 
            this.nbGroupes.Location = new System.Drawing.Point(135, 184);
            this.nbGroupes.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nbGroupes.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nbGroupes.Name = "nbGroupes";
            this.nbGroupes.Size = new System.Drawing.Size(219, 20);
            this.nbGroupes.TabIndex = 59;
            this.nbGroupes.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(39, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 58;
            this.label5.Text = "Nb groupes";
            // 
            // comboBoxEC
            // 
            this.comboBoxEC.FormattingEnabled = true;
            this.comboBoxEC.Location = new System.Drawing.Point(135, 259);
            this.comboBoxEC.Name = "comboBoxEC";
            this.comboBoxEC.Size = new System.Drawing.Size(219, 21);
            this.comboBoxEC.TabIndex = 61;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(39, 262);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 13);
            this.label6.TabIndex = 60;
            this.label6.Text = "EC";
            // 
            // FormAddSeance
            // 
            this.AcceptButton = this.b_valider;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 388);
            this.Controls.Add(this.comboBoxEC);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.nbGroupes);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxProf);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nbHeures);
            this.Controls.Add(this.comboBoxType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.b_cancel);
            this.Controls.Add(this.b_valider);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "FormAddSeance";
            this.Text = "Ajouter une séance";
            this.Load += new System.EventHandler(this.FormAddSeance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nbHeures)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nbGroupes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_cancel;
        private System.Windows.Forms.Button b_valider;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.NumericUpDown nbHeures;
        private System.Windows.Forms.ComboBox comboBoxProf;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nbGroupes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxEC;
        private System.Windows.Forms.Label label6;
    }
}