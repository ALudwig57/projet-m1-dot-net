﻿namespace Projet_dot_net_2019
{
    partial class FormAddPersonnel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.tb_forname = new System.Windows.Forms.TextBox();
            this.b_valider = new System.Windows.Forms.Button();
            this.b_cancel = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_mail = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.b_add_categorie = new System.Windows.Forms.Button();
            this.comboBox_categorie = new System.Windows.Forms.ComboBox();
            this.categoriesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.testDbCodeFirstDataSet = new Projet_dot_net_2019.TestDbCodeFirstDataSet();
            this.categoriesTableAdapter = new Projet_dot_net_2019.TestDbCodeFirstDataSetTableAdapters.CategoriesTableAdapter();
            this.categoriesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testDbCodeFirstDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(143, 71);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(100, 20);
            this.tb_name.TabIndex = 0;
            // 
            // tb_forname
            // 
            this.tb_forname.Location = new System.Drawing.Point(143, 111);
            this.tb_forname.Name = "tb_forname";
            this.tb_forname.Size = new System.Drawing.Size(100, 20);
            this.tb_forname.TabIndex = 1;
            // 
            // b_valider
            // 
            this.b_valider.Location = new System.Drawing.Point(280, 369);
            this.b_valider.Name = "b_valider";
            this.b_valider.Size = new System.Drawing.Size(80, 21);
            this.b_valider.TabIndex = 67;
            this.b_valider.Text = "Valider";
            this.b_valider.UseVisualStyleBackColor = true;
            this.b_valider.Click += new System.EventHandler(this.button1_Click);
            // 
            // b_cancel
            // 
            this.b_cancel.Location = new System.Drawing.Point(12, 369);
            this.b_cancel.Name = "b_cancel";
            this.b_cancel.Size = new System.Drawing.Size(80, 21);
            this.b_cancel.TabIndex = 68;
            this.b_cancel.Text = "Annuler";
            this.b_cancel.UseVisualStyleBackColor = true;
            this.b_cancel.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Nom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ajout d\'un nouveau personnel";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(40, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Prénom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Email";
            // 
            // tb_mail
            // 
            this.tb_mail.Location = new System.Drawing.Point(143, 153);
            this.tb_mail.Name = "tb_mail";
            this.tb_mail.Size = new System.Drawing.Size(100, 20);
            this.tb_mail.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 196);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Catégorie";
            // 
            // b_add_categorie
            // 
            this.b_add_categorie.Location = new System.Drawing.Point(265, 191);
            this.b_add_categorie.Name = "b_add_categorie";
            this.b_add_categorie.Size = new System.Drawing.Size(95, 23);
            this.b_add_categorie.TabIndex = 14;
            this.b_add_categorie.Text = "Ajout catégorie";
            this.b_add_categorie.UseVisualStyleBackColor = true;
            this.b_add_categorie.Click += new System.EventHandler(this.b_add_categorie_Click);
            // 
            // comboBox_categorie
            // 
            this.comboBox_categorie.FormattingEnabled = true;
            this.comboBox_categorie.Location = new System.Drawing.Point(143, 193);
            this.comboBox_categorie.Name = "comboBox_categorie";
            this.comboBox_categorie.Size = new System.Drawing.Size(100, 21);
            this.comboBox_categorie.TabIndex = 5;
            // 
            // categoriesBindingSource
            // 
            this.categoriesBindingSource.DataMember = "Categories";
            this.categoriesBindingSource.DataSource = this.testDbCodeFirstDataSet;
            // 
            // testDbCodeFirstDataSet
            // 
            this.testDbCodeFirstDataSet.DataSetName = "TestDbCodeFirstDataSet";
            this.testDbCodeFirstDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // categoriesTableAdapter
            // 
            this.categoriesTableAdapter.ClearBeforeFill = true;
            // 
            // categoriesBindingSource1
            // 
            this.categoriesBindingSource1.DataMember = "Categories";
            this.categoriesBindingSource1.DataSource = this.testDbCodeFirstDataSet;
            // 
            // FormAddPersonnel
            // 
            this.AcceptButton = this.b_valider;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(372, 402);
            this.ControlBox = false;
            this.Controls.Add(this.comboBox_categorie);
            this.Controls.Add(this.b_add_categorie);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tb_mail);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.b_cancel);
            this.Controls.Add(this.b_valider);
            this.Controls.Add(this.tb_forname);
            this.Controls.Add(this.tb_name);
            this.Name = "FormAddPersonnel";
            this.Text = "gtgt";
            this.Load += new System.EventHandler(this.Form_add_personnel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testDbCodeFirstDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.categoriesBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.TextBox tb_forname;
        private System.Windows.Forms.Button b_valider;
        private System.Windows.Forms.Button b_cancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_mail;
        private System.Windows.Forms.Label label6;
        private TestDbCodeFirstDataSet testDbCodeFirstDataSet;
        private System.Windows.Forms.BindingSource categoriesBindingSource;
        private TestDbCodeFirstDataSetTableAdapters.CategoriesTableAdapter categoriesTableAdapter;
        private System.Windows.Forms.Button b_add_categorie;
        private System.Windows.Forms.BindingSource categoriesBindingSource1;
        private System.Windows.Forms.ComboBox comboBox_categorie;
    }
}