﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019
{
    public partial class FormAddPersonnel : Form
    {
        private PersonnelService personnelService;
        private CategorieService categorieService;
        private Personnel personnel;

        public static IEnumerable<Control> GetAllControls(Control root)
        {
            var stack = new Stack<Control>();
            stack.Push(root);

            while (stack.Any())
            {
                var next = stack.Pop();
                foreach (Control child in next.Controls)
                    stack.Push(child);

                yield return next;
            }
        }

        public FormAddPersonnel(PersonnelService personnelService, CategorieService categorieService)
        {
            this.personnelService = personnelService;
            this.categorieService = categorieService;
            InitializeComponent();
        }

        public FormAddPersonnel(Personnel personnel, PersonnelService personnelService, CategorieService categorieService)
        {
            this.personnelService = personnelService;
            this.categorieService = categorieService;
            InitializeComponent();
            tb_name.Text = personnel.Nom;
            tb_forname.Text = personnel.Prenom;
            tb_mail.Text = personnel.Email;
            label3.Text = "Modification d'un personnel";
            b_valider.Text = "Modifier";
        }

        public FormAddPersonnel(int id, PersonnelService personnelService, CategorieService categorieService)
        {
            this.personnelService = personnelService;
            this.categorieService = categorieService;
            personnel = personnelService.Get(id);
            InitializeComponent();
            tb_name.Text = personnel.Nom;
            tb_forname.Text = personnel.Prenom;
            tb_mail.Text = personnel.Email;
            label3.Text = "Modification d'un personnel";
            b_valider.Text = "Modifier";
            this.Text = "Modfier un personnel";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach(TextBox tb in GetAllControls(this).OfType<TextBox>())
            {
                if (tb.Text == "")
                {
                    MessageBox.Show("Un champ de saisie est vide");
                    return;
                }
            }

            if (b_valider.Text == "Modifier")
            {
                personnelService.updatePersonnel(personnel.Id, tb_name.Text, tb_forname.Text,
                    tb_mail.Text, 0, ((ItemWithID) comboBox_categorie.SelectedItem).HiddenID);
            }
            else
            {
               
                personnelService.addPersonnel(tb_name.Text, tb_forname.Text, tb_mail.Text, 0,
                    ((ItemWithID) comboBox_categorie.SelectedItem).HiddenID);
            }

            this.Close();
        }

        private void updateCategorieComboBox()
        {
            comboBox_categorie.Items.Clear();
            int i = 0;
            foreach (Categorie categorie in categorieService.GetAllCategories().Distinct())
            {
                comboBox_categorie.Items.Add(new ItemWithID(categorie.ID, categorie.Nom));

                if (b_valider.Text == "Modifier")
                {
                    if (personnel.Categorie.ID == categorie.ID)
                        comboBox_categorie.SelectedItem = comboBox_categorie.Items[i];
                }
                else
                {
                    comboBox_categorie.SelectedIndex = 0;
                }

                i++;
            }

        }

        private void Form_add_personnel_Load(object sender, EventArgs e)
        {
            updateCategorieComboBox();
        }


        private void b_add_categorie_Click(object sender, EventArgs e)
        {

            FormAddCategorie fc = new FormAddCategorie(categorieService);
            fc.ShowDialog(this);
            updateCategorieComboBox();
            
        }

        private void label_id_Click(object sender, EventArgs e)
        {

        }
    }
}
