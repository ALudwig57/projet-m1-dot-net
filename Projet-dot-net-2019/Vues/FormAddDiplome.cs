﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Vues
{
    public partial class FormAddDiplome : Form
    {
        private int _currentId = -1;
        private DiplomeService diplomeService;
        private AnneeUnivService anneeUnivService;

        public FormAddDiplome(DiplomeService diplomeService, AnneeUnivService anneeUnivService)
        {
            this.diplomeService = diplomeService;
            this.anneeUnivService = anneeUnivService;
            InitializeComponent();
        }

        public FormAddDiplome(int id, DiplomeService diplomeService, AnneeUnivService anneeUnivService)
        {
            _currentId = id;
            this.diplomeService = diplomeService;
            this.anneeUnivService = anneeUnivService;
            Diplome diplome = diplomeService.Get(_currentId);
            InitializeComponent();
            tb_nom.Text = diplome.Nom;
            b_valider.Text = "Modifier";
            label3.Text = "Modification d'un diplôme";
            this.Text = "Modifier un diplôme";
        }

        private List<AnneeUniv> GetAnneesListByChecked(CheckedListBox.CheckedItemCollection clc)
        {
            List<AnneeUniv>  list = new List<AnneeUniv>();
            foreach (string libelle in clc)
            {
                list.Add(anneeUnivService.FindAnnee(x => x.year.Contains(libelle)).First<AnneeUniv>());
            }
            return list;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_currentId != -1)
            {
                diplomeService.updateDiplome(_currentId, tb_nom.Text, GetAnneesListByChecked(clb_annees.CheckedItems).ToList());
                _currentId = -1;
            }
            else
            {
                diplomeService.AddDiplome(tb_nom.Text, GetAnneesListByChecked(clb_annees.CheckedItems));
            }

            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormAddAnnee fa = new FormAddAnnee(anneeUnivService);
            fa.ShowDialog(this);
            FillCheckboxList();
        }

        private void FillCheckboxList()
        {
            clb_annees.Items.Clear();
            if (_currentId != -1)
            {
                Diplome diplome = diplomeService.Get(_currentId);

                var anneesAffectees = diplome.AnneeUnivs;
                foreach(AnneeUniv anneeAffectee in anneesAffectees)
                    clb_annees.Items.Add(anneeAffectee.year, true);
            }
            IEnumerable<AnneeUniv> anneesNonAffectees = anneeUnivService.GetAnneesOrphelines().ToList();
            foreach (AnneeUniv anneeNonAffectee in anneesNonAffectees)
            {
                clb_annees.Items.Add(anneeNonAffectee.year);
            }
        }

        private void Form_add_diplome_Load(object sender, EventArgs e)
        {
            FillCheckboxList();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void b_annuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
