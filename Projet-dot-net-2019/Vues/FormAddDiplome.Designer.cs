﻿namespace Projet_dot_net_2019.Vues
{
    partial class FormAddDiplome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.b_valider = new System.Windows.Forms.Button();
            this.b_annuler = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clb_annees = new System.Windows.Forms.CheckedListBox();
            this.tb_nom = new System.Windows.Forms.TextBox();
            this.b_create_annee = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Ajout d\'un nouveau diplôme";
            // 
            // b_valider
            // 
            this.b_valider.Location = new System.Drawing.Point(216, 335);
            this.b_valider.Name = "b_valider";
            this.b_valider.Size = new System.Drawing.Size(75, 23);
            this.b_valider.TabIndex = 8;
            this.b_valider.Text = "Valider";
            this.b_valider.UseVisualStyleBackColor = true;
            this.b_valider.Click += new System.EventHandler(this.button1_Click);
            // 
            // b_annuler
            // 
            this.b_annuler.Location = new System.Drawing.Point(12, 335);
            this.b_annuler.Name = "b_annuler";
            this.b_annuler.Size = new System.Drawing.Size(75, 23);
            this.b_annuler.TabIndex = 9;
            this.b_annuler.Text = "Annuler";
            this.b_annuler.UseVisualStyleBackColor = true;
            this.b_annuler.Click += new System.EventHandler(this.b_annuler_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nom : ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Nombre d\'années : ";
            // 
            // clb_annees
            // 
            this.clb_annees.CheckOnClick = true;
            this.clb_annees.FormattingEnabled = true;
            this.clb_annees.Location = new System.Drawing.Point(125, 218);
            this.clb_annees.Name = "clb_annees";
            this.clb_annees.Size = new System.Drawing.Size(166, 94);
            this.clb_annees.TabIndex = 12;
            // 
            // tb_nom
            // 
            this.tb_nom.Location = new System.Drawing.Point(125, 143);
            this.tb_nom.Name = "tb_nom";
            this.tb_nom.Size = new System.Drawing.Size(166, 20);
            this.tb_nom.TabIndex = 13;
            // 
            // b_create_annee
            // 
            this.b_create_annee.Location = new System.Drawing.Point(12, 275);
            this.b_create_annee.Name = "b_create_annee";
            this.b_create_annee.Size = new System.Drawing.Size(75, 37);
            this.b_create_annee.TabIndex = 14;
            this.b_create_annee.Text = "Créer une année";
            this.b_create_annee.UseVisualStyleBackColor = true;
            this.b_create_annee.Click += new System.EventHandler(this.button3_Click);
            // 
            // FormAddDiplome
            // 
            this.AcceptButton = this.b_valider;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 368);
            this.Controls.Add(this.b_create_annee);
            this.Controls.Add(this.tb_nom);
            this.Controls.Add(this.clb_annees);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.b_annuler);
            this.Controls.Add(this.b_valider);
            this.Controls.Add(this.label3);
            this.Name = "FormAddDiplome";
            this.Text = "Ajouter un diplôme";
            this.Load += new System.EventHandler(this.Form_add_diplome_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button b_valider;
        private System.Windows.Forms.Button b_annuler;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox clb_annees;
        private System.Windows.Forms.TextBox tb_nom;
        private System.Windows.Forms.Button b_create_annee;
    }
}