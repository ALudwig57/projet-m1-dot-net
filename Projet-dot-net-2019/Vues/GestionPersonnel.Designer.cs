﻿namespace Projet_dot_net_2019
{
    partial class GestionPersonnel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.b_add_personnel = new System.Windows.Forms.Button();
            this.b_update = new System.Windows.Forms.Button();
            this.b_delete = new System.Windows.Forms.Button();
            this.b_add_categorie = new System.Windows.Forms.Button();
            this.panel_fiche = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(93, 12);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(244, 426);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // b_add_personnel
            // 
            this.b_add_personnel.Location = new System.Drawing.Point(12, 12);
            this.b_add_personnel.Name = "b_add_personnel";
            this.b_add_personnel.Size = new System.Drawing.Size(75, 58);
            this.b_add_personnel.TabIndex = 1;
            this.b_add_personnel.Text = "Ajouter un personnel";
            this.b_add_personnel.UseVisualStyleBackColor = true;
            this.b_add_personnel.Click += new System.EventHandler(this.b_add_Click);
            // 
            // b_update
            // 
            this.b_update.Location = new System.Drawing.Point(12, 144);
            this.b_update.Name = "b_update";
            this.b_update.Size = new System.Drawing.Size(75, 23);
            this.b_update.TabIndex = 2;
            this.b_update.Text = "Modifier";
            this.b_update.UseVisualStyleBackColor = true;
            this.b_update.Click += new System.EventHandler(this.b_update_Click);
            // 
            // b_delete
            // 
            this.b_delete.Location = new System.Drawing.Point(12, 173);
            this.b_delete.Name = "b_delete";
            this.b_delete.Size = new System.Drawing.Size(75, 23);
            this.b_delete.TabIndex = 3;
            this.b_delete.Text = "Supprimer";
            this.b_delete.UseVisualStyleBackColor = true;
            this.b_delete.Click += new System.EventHandler(this.b_delete_Click);
            // 
            // b_add_categorie
            // 
            this.b_add_categorie.Location = new System.Drawing.Point(13, 77);
            this.b_add_categorie.Name = "b_add_categorie";
            this.b_add_categorie.Size = new System.Drawing.Size(75, 61);
            this.b_add_categorie.TabIndex = 4;
            this.b_add_categorie.Text = "Ajouter une catégorie";
            this.b_add_categorie.UseVisualStyleBackColor = true;
            this.b_add_categorie.Click += new System.EventHandler(this.b_add_categorie_Click);
            // 
            // panel_fiche
            // 
            this.panel_fiche.Location = new System.Drawing.Point(343, 12);
            this.panel_fiche.Name = "panel_fiche";
            this.panel_fiche.Size = new System.Drawing.Size(466, 426);
            this.panel_fiche.TabIndex = 5;
            // 
            // GestionPersonnel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(821, 450);
            this.Controls.Add(this.panel_fiche);
            this.Controls.Add(this.b_add_categorie);
            this.Controls.Add(this.b_delete);
            this.Controls.Add(this.b_update);
            this.Controls.Add(this.b_add_personnel);
            this.Controls.Add(this.treeView1);
            this.Name = "GestionPersonnel";
            this.Text = "Gestion du personnel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GestionPersonnel_FormClosing);
            this.Load += new System.EventHandler(this.GestionPersonnel_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button b_add_personnel;
        private System.Windows.Forms.Button b_update;
        private System.Windows.Forms.Button b_delete;
        private System.Windows.Forms.Button b_add_categorie;
        private System.Windows.Forms.Panel panel_fiche;
    }
}