﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Projet_dot_net_2019.Vues
{
    class OutilsVues
    {
        public System.Drawing.Color color = System.Drawing.Color.Black;
        public static void AjoutNoeudRacine(TreeNodeCollection nodes, string intitule, string key)
        {
            nodes.Add(key, intitule);
        }

        public static void AjoutNoeudEnfant(TreeNodeCollection nodes, string parentkey, string intitule, string key, bool searchChildrens, Color ? couleur = null )
        {
            TreeNode treeNode = nodes.Find(parentkey, searchChildrens).First();

            //treeNode.BackColor = couleur ?? Color.Black;
            //TreeNode node = new TreeNode(intitule);

            //treeNode.Nodes.Add(key,node);
           
            treeNode.Nodes.Add(key, intitule);

            TreeNode temp = treeNode.Nodes.Find(key, searchChildrens).First();
            temp.ForeColor = couleur ?? Color.Black;

        }


        /// <summary>
        ///     Execute le calcul des eq en fonction de leur format (fraction ou double avec ou sans virgule).
        /// </summary>
        /// <param name="eq">
        ///     la chaîne correspondant à l'eq
        /// </param>
        /// <returns>
        ///     Résultat du calcul en double.
        /// </returns>
        public static double calculEq(string eq)
        {
            if (eq.Contains('/'))
            {
                string[] elementStrings = eq.Split('/');
                if (elementStrings.Length == 2)
                {
                    if (double.Parse(elementStrings[1]) == 0)
                    {
                        throw new ArgumentException("Division par zéro impossible !");
                    }

                    return double.Parse(elementStrings[0]) / double.Parse(elementStrings[1]);
                }
                else
                    throw new ArgumentException("La valeur saisie est incorrect");
            }
            return double.Parse(eq);
        }
    }

    public class CheckboxWithID : CheckBox
    {
        private int _id;
        public int id
        {
            get
            {
                return _id;
            }
        }
        public CheckboxWithID(int id) : base()
        {
            this._id = id;
        }
    }
}
