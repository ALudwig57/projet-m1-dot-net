﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.AbstractFactories.Controllers;
using Projet_dot_net_2019.ControlsForm;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues;

namespace Projet_dot_net_2019
{
    public partial class GestionPersonnel : Form
    {
        private CategorieController categorieController;
        private PersonnelController personnelController;

        private GestionPersonnelController gestionPersonnelController;

        private Form parent;

        public GestionPersonnel(Form parent, List<IController> listeControllersPersonnel, GestionPersonnelController gestionPersonnelController)
        {
            //0: Categorie, 1: Personnel
            categorieController = (CategorieController)listeControllersPersonnel[0];
            personnelController = (PersonnelController)listeControllersPersonnel[1];

            this.gestionPersonnelController = gestionPersonnelController;

            this.parent = parent;
            InitializeComponent();
        }

        private void b_add_Click(object sender, EventArgs e)
        {
            FormAddPersonnel fp = (FormAddPersonnel) personnelController.CreateForm();
            fp.ShowDialog(this);
            //FillTree();
            gestionPersonnelController.FillTree(treeView1);

        }

        private void b_update_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(treeView1.SelectedNode.Name.Split('_')[1]);
            int niveau = treeView1.SelectedNode.Level;
            Form formEdit = NiveauController(niveau).EditForm(id);
            formEdit.ShowDialog(this);
            //FillTree();
            gestionPersonnelController.FillTree(treeView1);
        }

        private void b_delete_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show( "Êtes vous certain de votre suppression ? Cette action est irréversible !", "Confirmer la suppression", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                int niveau = treeView1.SelectedNode.Level;
                int id = Int32.Parse(treeView1.SelectedNode.Name.Split('_')[1]);
                NiveauController(niveau).delete(id);
                //FillTree();
                gestionPersonnelController.FillTree(treeView1);
            }
            
        }


        private void GestionPersonnel_Load(object sender, EventArgs e)
        {
            //FillTree();
            gestionPersonnelController.FillTree(treeView1);
            treeView1.ExpandAll();
        }

        private void b_add_categorie_Click(object sender, EventArgs e)
        {
            FormAddCategorie fc = (FormAddCategorie) categorieController.CreateForm();
            fc.ShowDialog(this);
            gestionPersonnelController.FillTree(treeView1);
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            panel_fiche.Controls.Clear();
            int niveau = treeView1.SelectedNode.Level;
            int id = Int32.Parse(treeView1.SelectedNode.Name.Split('_')[1]);
            //UserControl fiche = NiveauController(niveau).CreateFiche(Int32.Parse(treeView1.SelectedNode.Name));
            UserControl fiche = NiveauController(niveau).CreateFiche(id);
            panel_fiche.Controls.Add(fiche);
            fiche.Dock= DockStyle.Fill;
            fiche.BringToFront();
        }

        private IController NiveauController(int niveau)
        {
            switch (niveau)
            {
                case 0:
                    return categorieController;
                case 1:
                    return personnelController;
                default:
                    throw new NotSupportedException();
            }
        }

        private void GestionPersonnel_FormClosing(object sender, FormClosingEventArgs e)
        {
            parent.Show();
            this.Hide();
        }

        
    }
}
