﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Vues
{
    /// <summary>
    /// Formulaire d'ajout ou de modifiacation d'une année, dans le cas d'une mudification, les champs seront déjà pré remplis
    /// </summary>
    public partial class FormAddAnnee : Form
    {
        private int _currentId = -1;
        private AnneeUnivService anneeUnivService;

        /// <summary>
        /// Constructeur de formulaire d'ajout
        /// </summary>
        /// <param name="anneeUnivService"></param>
        public FormAddAnnee(AnneeUnivService anneeUnivService)
        {
            this.anneeUnivService = anneeUnivService;
            InitializeComponent();
        }

        /// <summary>
        /// Constructeur de formultaire de modification
        /// </summary>
        /// <param name="id">ID de l'année à modifier</param>
        /// <param name="anneeUnivService"></param>
        public FormAddAnnee(int id, AnneeUnivService anneeUnivService)
        {
            _currentId = id;
            this.anneeUnivService = anneeUnivService;
            AnneeUniv annee = this.anneeUnivService.Get(_currentId);
            InitializeComponent();
            textBox1.Text = annee.year;
            b_valider.Text = "Modifier";
            label3.Text = "Modification d'une année universitaire";
            this.Text = "Modifier une année";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //si le formulaire est un formulaire d'ajout
            if (_currentId != -1)
            {
                anneeUnivService.updateAnneeUniversitaire(_currentId, textBox1.Text);
            }
            //si le formulaire est un formulaire de modification
            else
            {
                anneeUnivService.addAnneeUniversitaire(textBox1.Text);
            }
            this.Close();
        }

        private void Form_add_annee_Load(object sender, EventArgs e)
        {

        }

        private void Form_add_annee_Load_1(object sender, EventArgs e)
        {

        }
    }
}
