﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using Projet_dot_net_2019.Vues.CORAddSeance;

namespace Projet_dot_net_2019.Vues
{
    public partial class FormAddSeance : Form
    {
        private SeanceService seanceService;
        private ECService ecService;
        private PersonnelService personnelService;

        private int _currentId = -1;

        public FormAddSeance(SeanceService seanceService, ECService ecService, PersonnelService personnelService)
        {
            this.seanceService = seanceService;
            this.ecService = ecService;
            this.personnelService = personnelService;
            InitializeComponent();
        }

        public FormAddSeance(int id, SeanceService seanceService, ECService ecService, PersonnelService personnelService)
        {
            _currentId = id;
            InitializeComponent();
            this.seanceService = seanceService;
            this.ecService = ecService;
            this.personnelService = personnelService;

            Seance seance = this.seanceService.Get(_currentId);
            this.nbHeures.Value = seance.NbHeures;
            if (seance.GetType() == typeof(Travaux))
                this.nbGroupes.Value = ((Travaux)seance).NbGroupes;

            
            label3.Text = "Modification d'un cours";
            this.Text = "Modifier une séance";
        }

        private void FillElements()
        {
            this.comboBoxType.Items.Clear();
            this.comboBoxProf.Items.Clear();

            this.comboBoxType.Items.Add(new ItemWithID(1, "CM"));
            this.comboBoxType.Items.Add(new ItemWithID(2, "TD"));
            this.comboBoxType.Items.Add(new ItemWithID(3, "TP"));
            this.comboBoxType.Items.Add(new ItemWithID(4, "EI"));
            this.comboBoxType.SelectedItem = comboBoxType.Items[0];

            int i = 0;
            foreach (Personnel personnel in personnelService.GetAllPersonnels())
            {
                comboBoxProf.Items.Add(new ItemWithID(personnel.Id, personnel.Prenom+' '+personnel.Nom));

                if (_currentId != -1)
                {
                    if (_currentId == personnel.Id)
                        comboBoxProf.SelectedItem = comboBoxProf.Items[i];
                }
                else
                {
                    comboBoxProf.SelectedIndex = 0;
                }

                i++;
            }

            i = 0;
            foreach (EC EC in ecService.GetEcs())
            {
                comboBoxEC.Items.Add(new ItemWithID(EC.ID, EC.Nom));

                if (_currentId != -1)
                {
                    if (_currentId == EC.ID)
                        comboBoxEC.SelectedItem = comboBoxEC.Items[i];
                }
                else
                {
                    comboBoxEC.SelectedIndex = 0;
                }

                i++;
            }
        }

        private void b_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void b_valider_Click(object sender, EventArgs e)
        {
            Seance seance = null;
            if (_currentId != -1)
            {
                seance = this.seanceService.Get(_currentId);

            }
            else
            {
                Adder CmAdder = new CmAdder();
                Adder TdAdder = new TdAdder();
                Adder TpAdder = new TpAdder();
                Adder EiAdder = new EiAdder();

                //Create the chain of responsability
                CmAdder.SetSupervisor(TdAdder);
                TdAdder.SetSupervisor(TpAdder);
                TpAdder.SetSupervisor(EiAdder);



                seance = CmAdder.ProcessRequest(this.comboBoxType.SelectedIndex, (int)this.nbHeures.Value, personnelService.Get(((ItemWithID)comboBoxProf.SelectedItem).HiddenID), ecService.Get(((ItemWithID)comboBoxEC.SelectedItem).HiddenID), (int)this.nbGroupes.Value);
                if (seance != null)
                {
                    this.seanceService.Add(seance);
                    this.ecService.Update(seance.EC);
                    this.Close();
                }
               

            }

        }

        private void FormAddSeance_Load(object sender, EventArgs e)
        {
            FillElements();
        }
    }
}
