﻿namespace Projet_dot_net_2019.Vues
{
    partial class GestionCours
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.panel_fiche = new System.Windows.Forms.Panel();
            this.b_add_diplome = new System.Windows.Forms.Button();
            this.b_delete = new System.Windows.Forms.Button();
            this.b_update = new System.Windows.Forms.Button();
            this.b_add_annee = new System.Windows.Forms.Button();
            this.b_add_ue = new System.Windows.Forms.Button();
            this.b_add_cours = new System.Windows.Forms.Button();
            this.b_add_ec = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(94, 12);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(308, 426);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // panel_fiche
            // 
            this.panel_fiche.Location = new System.Drawing.Point(419, 12);
            this.panel_fiche.Name = "panel_fiche";
            this.panel_fiche.Size = new System.Drawing.Size(466, 426);
            this.panel_fiche.TabIndex = 1;
            this.panel_fiche.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // b_add_diplome
            // 
            this.b_add_diplome.Location = new System.Drawing.Point(12, 76);
            this.b_add_diplome.Name = "b_add_diplome";
            this.b_add_diplome.Size = new System.Drawing.Size(75, 61);
            this.b_add_diplome.TabIndex = 8;
            this.b_add_diplome.Text = "Ajouter un diplôme";
            this.b_add_diplome.UseVisualStyleBackColor = true;
            this.b_add_diplome.Click += new System.EventHandler(this.b_add_diplome_Click);
            // 
            // b_delete
            // 
            this.b_delete.Location = new System.Drawing.Point(12, 343);
            this.b_delete.Name = "b_delete";
            this.b_delete.Size = new System.Drawing.Size(75, 23);
            this.b_delete.TabIndex = 7;
            this.b_delete.Text = "Supprimer";
            this.b_delete.UseVisualStyleBackColor = true;
            this.b_delete.Click += new System.EventHandler(this.b_delete_Click_1);
            // 
            // b_update
            // 
            this.b_update.Location = new System.Drawing.Point(12, 314);
            this.b_update.Name = "b_update";
            this.b_update.Size = new System.Drawing.Size(75, 23);
            this.b_update.TabIndex = 6;
            this.b_update.Text = "Modifier";
            this.b_update.UseVisualStyleBackColor = true;
            this.b_update.Click += new System.EventHandler(this.b_update_Click);
            // 
            // b_add_annee
            // 
            this.b_add_annee.Location = new System.Drawing.Point(12, 12);
            this.b_add_annee.Name = "b_add_annee";
            this.b_add_annee.Size = new System.Drawing.Size(75, 58);
            this.b_add_annee.TabIndex = 5;
            this.b_add_annee.Text = "Ajouter une année";
            this.b_add_annee.UseVisualStyleBackColor = true;
            this.b_add_annee.Click += new System.EventHandler(this.b_add_annee_Click);
            // 
            // b_add_ue
            // 
            this.b_add_ue.Location = new System.Drawing.Point(12, 143);
            this.b_add_ue.Name = "b_add_ue";
            this.b_add_ue.Size = new System.Drawing.Size(75, 61);
            this.b_add_ue.TabIndex = 9;
            this.b_add_ue.Text = "Ajouter une UE";
            this.b_add_ue.UseVisualStyleBackColor = true;
            this.b_add_ue.Click += new System.EventHandler(this.b_add_ue_Click);
            // 
            // b_add_cours
            // 
            this.b_add_cours.Location = new System.Drawing.Point(12, 262);
            this.b_add_cours.Name = "b_add_cours";
            this.b_add_cours.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.b_add_cours.Size = new System.Drawing.Size(75, 46);
            this.b_add_cours.TabIndex = 10;
            this.b_add_cours.Text = "Ajouter un cours";
            this.b_add_cours.UseVisualStyleBackColor = true;
            this.b_add_cours.Click += new System.EventHandler(this.b_add_cours_Click);
            // 
            // b_add_ec
            // 
            this.b_add_ec.Location = new System.Drawing.Point(12, 210);
            this.b_add_ec.Name = "b_add_ec";
            this.b_add_ec.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.b_add_ec.Size = new System.Drawing.Size(75, 46);
            this.b_add_ec.TabIndex = 11;
            this.b_add_ec.Text = "Ajouter une EC";
            this.b_add_ec.UseVisualStyleBackColor = true;
            this.b_add_ec.Click += new System.EventHandler(this.b_add_ec_Click);
            // 
            // GestionCours
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 450);
            this.Controls.Add(this.b_add_ec);
            this.Controls.Add(this.b_add_cours);
            this.Controls.Add(this.b_add_ue);
            this.Controls.Add(this.b_add_diplome);
            this.Controls.Add(this.b_delete);
            this.Controls.Add(this.b_update);
            this.Controls.Add(this.b_add_annee);
            this.Controls.Add(this.panel_fiche);
            this.Controls.Add(this.treeView1);
            this.Name = "GestionCours";
            this.Text = "Gestion des cours";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GestionCours_FormClosing);
            this.Load += new System.EventHandler(this.Gestion_Cours_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Panel panel_fiche;
        private System.Windows.Forms.Button b_add_diplome;
        private System.Windows.Forms.Button b_delete;
        private System.Windows.Forms.Button b_update;
        private System.Windows.Forms.Button b_add_annee;
        private System.Windows.Forms.Button b_add_ue;
        private System.Windows.Forms.Button b_add_cours;
        private System.Windows.Forms.Button b_add_ec;
    }
}