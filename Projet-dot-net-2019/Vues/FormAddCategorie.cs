﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;
using System.Text.RegularExpressions;

namespace Projet_dot_net_2019
{
    /// <summary>
    /// Formulaire d'ajout ou de modifiacation d'une catégorie, dans le cas d'une mudification, les champs seront déjà pré remplis
    /// </summary>
    public partial class FormAddCategorie : Form
    {
        private CategorieService categorieService;

        /// <summary>
        ///     Execute le calcul des eq en fonction de leur format (fraction ou double avec ou sans virgule).
        /// </summary>
        /// <param name="eq">
        ///     la chaîne correspondant à l'eq
        /// </param>
        /// <returns>
        ///     Résultat du calcul en double.
        /// </returns>
        public double calculEq(string eq)
        {
            if (eq.Contains('/'))
            {
                string[] elementStrings = eq.Split('/');
                if (elementStrings.Length == 2)
                {
                    if (double.Parse(elementStrings[1]) == 0)
                    {
                        throw new ArgumentException("Division par zéro impossible !");
                    }

                    return double.Parse(elementStrings[0]) / double.Parse(elementStrings[1]);
                }
                else 
                    throw new ArgumentException("La valeur saisie est incorrect");
            }
                return double.Parse(eq);
        }

        /// <summary>
        ///     Vérifie le format de l'eq (10 maximum, virgule ou slash, puis chiffre de 1 à 10
        /// </summary>
        /// <param name="eq">
        ///     Eq à vérifier
        /// </param>
        /// <returns>
        ///     True si format respecté, False sinon.
        /// </returns>
        private bool CheckEq(string eq)
        {
            System.Text.RegularExpressions.Regex regexEq = new Regex(@"^(10|[0-9])(\,|/|\.)?(10|[1-9])?$");
            return regexEq.IsMatch(eq);
        }

        public static IEnumerable<Control> GetAllControls(Control root)
        {
            var stack = new Stack<Control>();
            stack.Push(root);

            while (stack.Any())
            {
                var next = stack.Pop();
                foreach (Control child in next.Controls)
                    stack.Push(child);

                yield return next;
            }
        }


        /// <summary>
        /// Constructeur formulaire d'ajout
        /// </summary>
        /// <param name="categorieService"></param>
        public FormAddCategorie(CategorieService categorieService)
        {
            this.categorieService = categorieService;
            InitializeComponent();
        }

        /// <summary>
        /// Constructeur formulaire de modification
        /// </summary>
        /// <param name="id"></param>
        /// <param name="categorieService"></param>
        public FormAddCategorie(int id, CategorieService categorieService)
        {
            this.categorieService = categorieService;
            Categorie categorie = categorieService.Get(id);
            InitializeComponent();
            nom_categorie.Text = categorie.Nom;
            nb_heures.Value = categorie.NbHeures;
            tb_eqCM.Text = categorie.EqCM;
            tb_eqTP.Text = categorie.EqTP;
            tb_eqEI.Text = categorie.EqEI;
            label_id.Text = categorie.ID.ToString();
            b_valider.Text = "Modifier";
            this.Text = "Modifier une catégorie";

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void b_valider_Click(object sender, EventArgs e)
        {
            List<TextBox> champs = new List<TextBox>();
            champs.Add(this.tb_eqCM);
            champs.Add(this.tb_eqTP);
            champs.Add(this.tb_eqEI);
            foreach (TextBox textBox in champs)
            {
                if(!CheckEq(textBox.Text))
                {
                    MessageBox.Show("Equivalences non valide !");
                    return;
                }
            }
            if (b_valider.Text == "Modifier")
                categorieService.updateCategorie(Int32.Parse(label_id.Text), nom_categorie.Text, (int) nb_heures.Value, tb_eqCM.Text, tb_eqTP.Text, tb_eqEI.Text);
            else
                categorieService.addCategorie(nom_categorie.Text, (int) nb_heures.Value, tb_eqCM.Text, tb_eqTP.Text, tb_eqEI.Text);
            this.Close();
        }

        private void b_annuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormAddCategorie_Load(object sender, EventArgs e)
        {

        }
    }
}
