﻿namespace Projet_dot_net_2019.Vues
{
    partial class FormAddUE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_Semestre = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.b_cancel = new System.Windows.Forms.Button();
            this.b_valider = new System.Windows.Forms.Button();
            this.tb_name = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.clb_ecs = new System.Windows.Forms.CheckedListBox();
            this.addEC = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBox_Semestre
            // 
            this.comboBox_Semestre.FormattingEnabled = true;
            this.comboBox_Semestre.Location = new System.Drawing.Point(138, 154);
            this.comboBox_Semestre.Name = "comboBox_Semestre";
            this.comboBox_Semestre.Size = new System.Drawing.Size(219, 21);
            this.comboBox_Semestre.TabIndex = 30;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 162);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 28;
            this.label6.Text = "Semestre : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(113, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(121, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "Ajout d\'une nouvelle UE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Nom";
            // 
            // b_cancel
            // 
            this.b_cancel.Location = new System.Drawing.Point(9, 368);
            this.b_cancel.Name = "b_cancel";
            this.b_cancel.Size = new System.Drawing.Size(80, 21);
            this.b_cancel.TabIndex = 19;
            this.b_cancel.Text = "Annuler";
            this.b_cancel.UseVisualStyleBackColor = true;
            this.b_cancel.Click += new System.EventHandler(this.b_cancel_Click);
            // 
            // b_valider
            // 
            this.b_valider.Location = new System.Drawing.Point(277, 368);
            this.b_valider.Name = "b_valider";
            this.b_valider.Size = new System.Drawing.Size(80, 21);
            this.b_valider.TabIndex = 18;
            this.b_valider.Text = "Valider";
            this.b_valider.UseVisualStyleBackColor = true;
            this.b_valider.Click += new System.EventHandler(this.b_valider_Click);
            // 
            // tb_name
            // 
            this.tb_name.Location = new System.Drawing.Point(138, 115);
            this.tb_name.Name = "tb_name";
            this.tb_name.Size = new System.Drawing.Size(219, 20);
            this.tb_name.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 214);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "ECs : ";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // clb_ecs
            // 
            this.clb_ecs.CheckOnClick = true;
            this.clb_ecs.FormattingEnabled = true;
            this.clb_ecs.Location = new System.Drawing.Point(138, 214);
            this.clb_ecs.Name = "clb_ecs";
            this.clb_ecs.Size = new System.Drawing.Size(224, 109);
            this.clb_ecs.TabIndex = 32;
            // 
            // addEC
            // 
            this.addEC.Location = new System.Drawing.Point(38, 249);
            this.addEC.Name = "addEC";
            this.addEC.Size = new System.Drawing.Size(75, 37);
            this.addEC.TabIndex = 33;
            this.addEC.Text = "Créer une EC";
            this.addEC.UseVisualStyleBackColor = true;
            this.addEC.Click += new System.EventHandler(this.addEC_Click);
            // 
            // FormAddUE
            // 
            this.AcceptButton = this.b_valider;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 401);
            this.Controls.Add(this.addEC);
            this.Controls.Add(this.clb_ecs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBox_Semestre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.b_cancel);
            this.Controls.Add(this.b_valider);
            this.Controls.Add(this.tb_name);
            this.Name = "FormAddUE";
            this.Text = "Ajouter une UE";
            this.Load += new System.EventHandler(this.Form_add_ue_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_Semestre;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button b_cancel;
        private System.Windows.Forms.Button b_valider;
        private System.Windows.Forms.TextBox tb_name;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckedListBox clb_ecs;
        private System.Windows.Forms.Button addEC;
    }
}