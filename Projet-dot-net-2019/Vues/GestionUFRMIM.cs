﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.AbstractFactories.Controllers;

namespace Projet_dot_net_2019.Vues
{
    public partial class GestionUFRMIM : Form
    {
        private List<IController> listeControllersPersonnel;
        private List<IController> listeControllersCours;

        private GestionCoursController gestionCoursController;
        private GestionPersonnelController gestionPersonnelController;

        public GestionUFRMIM(List<IController> listeControllersPersonnel, List<IController> listeControllersCours, GestionPersonnelController gestionPersonnelController, GestionCoursController gestionCoursController)
        {
            this.listeControllersPersonnel = listeControllersPersonnel;
            this.listeControllersCours = listeControllersCours;
            this.gestionPersonnelController = gestionPersonnelController;
            this.gestionCoursController = gestionCoursController;

            InitializeComponent();
        }

        private void GestionUFRMIM_Load(object sender, EventArgs e)
        {

        }

        private void gestionCours_Click(object sender, EventArgs e)
        {
            GestionCours fenetre = new GestionCours(this, listeControllersCours, gestionCoursController);
            fenetre.Show();
            this.Hide();
        }

        private void gestionPersonnel_Click(object sender, EventArgs e)
        {
            GestionPersonnel fenetre = new GestionPersonnel(this, listeControllersPersonnel, gestionPersonnelController);
            fenetre.Show();
            this.Hide();
        }
    }
}
