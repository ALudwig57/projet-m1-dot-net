﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Vues
{
    public partial class FormAddSemestre : Form
    {
        private int _currentId = -1;
        private SemestreService semestreService;
        private UEService ueService;
        private AnneeUnivService anneeUnivService;
        private ECService ecService;
        private SeanceService seanceService;
        private Semestre semestre;

        public FormAddSemestre(int id, SemestreService semestreService, UEService ueService, AnneeUnivService anneeUnivService, ECService ecService, SeanceService seanceService)
        {
            _currentId = id;
            this.semestreService = semestreService;
            this.ueService = ueService;
            this.ueService.SemestreService = semestreService;
            this.anneeUnivService = anneeUnivService;
            this.ecService = ecService;
            this.seanceService = seanceService;
            this.semestre = this.semestreService.Get(_currentId);
            InitializeComponent();
            nomSemestre.Text = semestre.name;
        }

        private List<UE> GetUEListByChecked(CheckedListBox.CheckedItemCollection clc)
        {
            List<UE> list = new List<UE>();
            foreach (string libelle in clc)
            {
                //list.Add(ueService.FindSemestre(x => x.year.Contains(libelle)).First<UE>());
            }
            return list;
        }

        private void FillCheckboxList()
        {
            /*clb_UEs.Items.Clear();
            if (_currentId != -1)
            {
                var UEsAffectees = semestre.UEs;
                foreach (UE UEAffectee in UEsAffectees)
                    clb_UEs.Items.Add(UEAffectee.Nom, true);
            }
            IEnumerable<UE> UEsNonAffectees = ueService.GetUEsOrphelines().ToList();
            foreach (UE ueNonAffectee in UEsNonAffectees)
            {
                clb_UEs.Items.Add(ueNonAffectee.Nom);
            }*/
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void tb_nom_TextChanged(object sender, EventArgs e)
        {

        }

        private void clb_annees_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void b_annuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void b_valider_Click(object sender, EventArgs e)
        {
            //update semestre
            semestreService.UpdateSemestre(this.semestre);

            this.Close();
        }

        private void b_create_annee_Click(object sender, EventArgs e)
        {
            FormAddUE fa = new FormAddUE(ueService, ecService, semestreService, seanceService);
            fa.ShowDialog(this);
            FillCheckboxList();
        }

        private void FormAddSemestre_Load(object sender, EventArgs e)
        {
            FillCheckboxList();
        }
    }
}
