﻿namespace Projet_dot_net_2019.Vues
{
    partial class FormAddSemestre
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_create_UE = new System.Windows.Forms.Button();
            this.clb_UEs = new System.Windows.Forms.CheckedListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.b_annuler = new System.Windows.Forms.Button();
            this.b_valider = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.nomSemestre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // b_create_UE
            // 
            this.b_create_UE.Location = new System.Drawing.Point(69, 262);
            this.b_create_UE.Name = "b_create_UE";
            this.b_create_UE.Size = new System.Drawing.Size(75, 37);
            this.b_create_UE.TabIndex = 22;
            this.b_create_UE.Text = "Créer une UE";
            this.b_create_UE.UseVisualStyleBackColor = true;
            this.b_create_UE.Click += new System.EventHandler(this.b_create_annee_Click);
            // 
            // clb_UEs
            // 
            this.clb_UEs.FormattingEnabled = true;
            this.clb_UEs.Location = new System.Drawing.Point(182, 205);
            this.clb_UEs.Name = "clb_UEs";
            this.clb_UEs.Size = new System.Drawing.Size(166, 94);
            this.clb_UEs.TabIndex = 20;
            this.clb_UEs.SelectedIndexChanged += new System.EventHandler(this.clb_annees_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 205);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Nombre d\'UE :";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // b_annuler
            // 
            this.b_annuler.Location = new System.Drawing.Point(69, 322);
            this.b_annuler.Name = "b_annuler";
            this.b_annuler.Size = new System.Drawing.Size(75, 23);
            this.b_annuler.TabIndex = 17;
            this.b_annuler.Text = "Annuler";
            this.b_annuler.UseVisualStyleBackColor = true;
            this.b_annuler.Click += new System.EventHandler(this.b_annuler_Click);
            // 
            // b_valider
            // 
            this.b_valider.Location = new System.Drawing.Point(273, 322);
            this.b_valider.Name = "b_valider";
            this.b_valider.Size = new System.Drawing.Size(75, 23);
            this.b_valider.TabIndex = 16;
            this.b_valider.Text = "Modifier";
            this.b_valider.UseVisualStyleBackColor = true;
            this.b_valider.Click += new System.EventHandler(this.b_valider_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(131, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Editer un semestre";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // nomSemestre
            // 
            this.nomSemestre.AutoSize = true;
            this.nomSemestre.Location = new System.Drawing.Point(88, 117);
            this.nomSemestre.Name = "nomSemestre";
            this.nomSemestre.Size = new System.Drawing.Size(0, 13);
            this.nomSemestre.TabIndex = 23;
            // 
            // FormAddSemestre
            // 
            this.AcceptButton = this.b_valider;
            this.ClientSize = new System.Drawing.Size(417, 386);
            this.Controls.Add(this.nomSemestre);
            this.Controls.Add(this.b_create_UE);
            this.Controls.Add(this.clb_UEs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.b_annuler);
            this.Controls.Add(this.b_valider);
            this.Controls.Add(this.label3);
            this.Name = "FormAddSemestre";
            this.Text = "Semestre";
            this.Load += new System.EventHandler(this.FormAddSemestre_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_create_UE;
        private System.Windows.Forms.CheckedListBox clb_UEs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button b_annuler;
        private System.Windows.Forms.Button b_valider;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label nomSemestre;
    }
}
