﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Repositories
{
    public class AnneeUnivRepository:Repository<AnneeUniv>
    {
        public AnneeUnivRepository(ApplicationDbContext context) : base(context)
        {
        }

    }
}
