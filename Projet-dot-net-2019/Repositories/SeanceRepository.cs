﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Repositories
{
    class SeanceRepository : Repository<Seance>
    {
        public SeanceRepository(ApplicationDbContext context) : base(context)
        {
        }

        public Personnel GetPersonnel(int seanceID)
        {
            return base.Get(seanceID).Personnel;
        }
    }
}
