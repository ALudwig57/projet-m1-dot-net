﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Repositories
{
    public class UERepository:Repository<UE>
    {
        public UERepository(ApplicationDbContext context) : base(context)
        {
        }
    }
}
