﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Repositories
{
    /// <summary>
    /// Classe abstraite permettant de fournir les fonctions d'ajout, de suppression, de consultation, de modification, et de sauvegarde dans la base de données
    /// </summary>
    /// <typeparam name="TEntity">Modèle</typeparam>
    public class Repository<TEntity> : IDisposable, IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Permet de gérer la connexion entre le code source et la base
        /// </summary>
        protected readonly ApplicationDbContext _context;
        /// <summary>
        /// ORM du modèle concerné
        /// </summary>
        protected DbSet<TEntity> dbSet;

        public ApplicationDbContext Context => _context;

        /// <summary>
        /// Constructeur du repository : Initialisation de la DbSet et du context
        /// </summary>
        /// <param name="context"></param>
        public Repository(ApplicationDbContext context)
        {
            _context = context;
            dbSet = _context.Set<TEntity>();
        }

        /// <summary>
        /// Ajoute une entrée à la base
        /// </summary>
        /// <param name="entity">Entrée à ajouter à la base</param>
        public void Add(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }

        /// <summary>
        /// Supprime une entrée de la base
        /// </summary>
        /// <param name="entity">Entrée à supprimer de la base</param>
        public void Delete(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
        }

        /// <summary>
        /// Récupère toutes les entrées de la table concernée sous forme de liste
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().ToList();
        }

        /// <summary>
        /// Récupère une entrée en fonction de sa clef primaire
        /// similaire à SELECT * FROM table where id = id en paramètre
        /// </summary>
        /// <param name="id">Id de l'entrée à récupérer</param>
        /// <returns>L'entrée</returns>
        public TEntity Get(int id)
        {
            return _context.Set<TEntity>().Find(id);
        }


        /// <summary>
        /// Trouve des entrées avec une requête Where
        /// </summary>
        /// <param name="predicate">Condition Where</param>
        /// <returns>Liste ds éléments qui répondent à la requête</returns>
        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate);
        }

        /// <summary>
        /// Mets à jour l'entrée dans la table
        /// </summary>
        /// <param name="entity"></param>
        public void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }

        /// <summary>
        /// Sauvegarde les changements dans la BDD
        /// </summary>
        /// <returns>1 si succès, 0 sinon</returns>
        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
