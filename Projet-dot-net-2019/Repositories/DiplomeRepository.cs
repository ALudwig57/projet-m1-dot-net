﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;

namespace Projet_dot_net_2019.Repositories
{
    class DiplomeRepository : Repository<Diplome>
    {
        public DiplomeRepository(ApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Attache une année au contexte (en cas de modification
        /// </summary>
        /// <param name="annee">Année à attacher</param>
        public void AttachAnnee(AnneeUniv annee)
        {
            this.Context.anneeUniv.Attach(annee);
        }
    }
}
