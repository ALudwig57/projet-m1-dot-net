﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.ControlsForm
{
    /// <summary>
    /// Fiche pour afficher les informations d'un personnel
    /// </summary>
    public partial class FichePersonnel : UserControl
    {
        /// <summary>
        /// Personnel concerné
        /// </summary>
        private Personnel personnel;

        /// <summary>
        /// Constructeur de la fiche
        /// </summary>
        /// <param name="id">Id du personnel concerné</param>
        /// <param name="service">Service du personnel</param>
        public  FichePersonnel(int id, PersonnelService service)
        {
            personnel = service.Get(id);
            InitializeComponent();
            l_nom_pers.Text = personnel.Nom.ToUpper() + " " + personnel.Prenom;
            l_email.Text = personnel.Email;

            double nbHeuresTotales = personnel.Categorie.NbHeures;
            l_nb_h.Text = personnel.NbHeuresEffectives + " h / " + nbHeuresTotales.ToString() +" h";
            if (nbHeuresTotales < personnel.NbHeuresEffectives)
            {
                l_nb_h.ForeColor = Color.Red;
            }
            else { l_nb_h.ForeColor = Color.Green; }

            


            l_category.Text = service.GetCategorieOfPersonnel(id).Nom;
        }

        

        private void FichePersonnel_Load(object sender, EventArgs e)
        {

        }

        private void l_nb_h_Click(object sender, EventArgs e)
        {

        }
    }
}
