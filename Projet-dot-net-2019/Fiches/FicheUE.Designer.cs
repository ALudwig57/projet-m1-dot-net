﻿namespace Projet_dot_net_2019.ControlsForm
{
    partial class FicheUE
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_nom_ue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.l_annee = new System.Windows.Forms.Label();
            this.list_personnels = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // l_nom_ue
            // 
            this.l_nom_ue.AutoSize = true;
            this.l_nom_ue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nom_ue.Location = new System.Drawing.Point(23, 25);
            this.l_nom_ue.Name = "l_nom_ue";
            this.l_nom_ue.Size = new System.Drawing.Size(76, 25);
            this.l_nom_ue.TabIndex = 10;
            this.l_nom_ue.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 282);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Année concernée : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(25, 324);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(166, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Enseignants responsables : ";
            // 
            // l_annee
            // 
            this.l_annee.AutoSize = true;
            this.l_annee.Location = new System.Drawing.Point(238, 282);
            this.l_annee.Name = "l_annee";
            this.l_annee.Size = new System.Drawing.Size(35, 13);
            this.l_annee.TabIndex = 18;
            this.l_annee.Text = "label2";
            // 
            // list_personnels
            // 
            this.list_personnels.FormattingEnabled = true;
            this.list_personnels.Location = new System.Drawing.Point(241, 324);
            this.list_personnels.Name = "list_personnels";
            this.list_personnels.Size = new System.Drawing.Size(214, 95);
            this.list_personnels.TabIndex = 19;
            // 
            // FicheUE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.list_personnels);
            this.Controls.Add(this.l_annee);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.l_nom_ue);
            this.Name = "FicheUE";
            this.Size = new System.Drawing.Size(466, 426);
            this.Load += new System.EventHandler(this.FicheUE_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_nom_ue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label l_annee;
        private System.Windows.Forms.ListBox list_personnels;
    }
}
