﻿namespace Projet_dot_net_2019.ControlsForm
{
    partial class FicheDiplome
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_nom_diplome = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.list_annees = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // l_nom_diplome
            // 
            this.l_nom_diplome.AutoSize = true;
            this.l_nom_diplome.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nom_diplome.Location = new System.Drawing.Point(17, 23);
            this.l_nom_diplome.Name = "l_nom_diplome";
            this.l_nom_diplome.Size = new System.Drawing.Size(76, 25);
            this.l_nom_diplome.TabIndex = 1;
            this.l_nom_diplome.Text = "label1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(19, 288);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Années : ";
            // 
            // list_annees
            // 
            this.list_annees.FormattingEnabled = true;
            this.list_annees.Location = new System.Drawing.Point(320, 288);
            this.list_annees.Name = "list_annees";
            this.list_annees.Size = new System.Drawing.Size(120, 95);
            this.list_annees.TabIndex = 6;
            // 
            // FicheDiplome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.list_annees);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.l_nom_diplome);
            this.Name = "FicheDiplome";
            this.Size = new System.Drawing.Size(466, 426);
            this.Load += new System.EventHandler(this.FicheDiplome_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_nom_diplome;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox list_annees;
    }
}
