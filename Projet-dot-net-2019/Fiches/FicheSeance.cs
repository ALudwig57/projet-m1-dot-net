﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Fiches
{
    /// <summary>
    /// Fiche permettant d'afficher les informations d'une séance
    /// </summary>
    public partial class FicheSeance : UserControl
    {
        private ECService ecService;
        private SeanceService seanceService;

        /// <summary>
        /// Constructeur de la fiche
        /// </summary>
        /// <param name="id">ID de la séance concernée</param>
        /// <param name="seanceService">Service de la séance concernée</param>
        /// <param name="ecService">Service de l'EC correspondant à la séance</param>
        public FicheSeance(int id, SeanceService seanceService, ECService ecService)
        {
            this.ecService = ecService;
            this.seanceService = seanceService;
            InitializeComponent();
            Seance seance = this.seanceService.Get(id);
            this.ec.Text = seance.EC.Nom;
            this.type.Text = seance.TypeCours;
            this.nbHeures.Text = seance.NbHeures.ToString();
            this.nbGroupes.Text = "1";
            if (seance.TypeCours != "CM")
                this.nbGroupes.Text = ((Travaux) seance).NbGroupes.ToString();
            if (seance.Personnel != null)
                this.prof.Text = seance.Personnel.Nom + ' ' + seance.Personnel.Prenom;
            
        }

        private void FicheSeance_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ec_Click(object sender, EventArgs e)
        {

        }
    }
}
