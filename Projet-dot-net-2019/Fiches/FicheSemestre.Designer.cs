﻿namespace Projet_dot_net_2019.ControlsForm
{
    partial class FicheSemestre
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_nom_semestre = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.listBox_UEs_Semestre = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.l_heures_total = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // l_nom_semestre
            // 
            this.l_nom_semestre.AutoSize = true;
            this.l_nom_semestre.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nom_semestre.Location = new System.Drawing.Point(21, 24);
            this.l_nom_semestre.Name = "l_nom_semestre";
            this.l_nom_semestre.Size = new System.Drawing.Size(76, 25);
            this.l_nom_semestre.TabIndex = 1;
            this.l_nom_semestre.Text = "label1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(203, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "UEs appartenants à ce semestre : ";
            // 
            // listBox_UEs_Semestre
            // 
            this.listBox_UEs_Semestre.FormattingEnabled = true;
            this.listBox_UEs_Semestre.Location = new System.Drawing.Point(233, 316);
            this.listBox_UEs_Semestre.Name = "listBox_UEs_Semestre";
            this.listBox_UEs_Semestre.Size = new System.Drawing.Size(219, 95);
            this.listBox_UEs_Semestre.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Nombre d\'heures total : ";
            // 
            // l_heures_total
            // 
            this.l_heures_total.AutoSize = true;
            this.l_heures_total.Location = new System.Drawing.Point(233, 280);
            this.l_heures_total.Name = "l_heures_total";
            this.l_heures_total.Size = new System.Drawing.Size(35, 13);
            this.l_heures_total.TabIndex = 9;
            this.l_heures_total.Text = "label2";
            // 
            // FicheSemestre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.l_heures_total);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox_UEs_Semestre);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.l_nom_semestre);
            this.Name = "FicheSemestre";
            this.Size = new System.Drawing.Size(466, 426);
            this.Load += new System.EventHandler(this.FicheSemestre_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_nom_semestre;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBox_UEs_Semestre;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label l_heures_total;
    }
}
