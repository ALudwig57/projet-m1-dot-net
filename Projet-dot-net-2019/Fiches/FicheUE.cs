﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.ControlsForm
{
    /// <summary>
    /// Fiche affichant les informations de l'UE 
    /// </summary>
    public partial class FicheUE : UserControl
    {
        //Service de l'annee universitaire
        private AnneeUnivService anneeUnivService;
        //Service de l'UE
        private UEService ueService;

        //UE concernée
        private UE ue;

        /// <summary>
        /// Constructeur de la fiche de l'UE
        /// </summary>
        /// <param name="id">ID de l'UE concernée</param>
        /// <param name="ueService">Service de l'UE</param>
        /// <param name="anneeUnivService">Service de l'annéeUnivService</param>
        public FicheUE(int id, UEService ueService, AnneeUnivService anneeUnivService)
        {
            this.anneeUnivService = anneeUnivService;
            this.ueService = ueService;

            ue = ueService.Get(id);
            InitializeComponent();
            
            l_nom_ue.Text = ue.Nom;
            l_annee.Text = anneeUnivService.GetAnneeOfSemestre(ue.Semestre).year;
        }

        private void FicheUE_Load(object sender, EventArgs e)
        {
            FillProfs();
        }

        /// <summary>
        /// Remplit la fiche du personnel
        /// </summary>
        private void FillProfs()
        {
            IEnumerable<Personnel> profs = ue.Profs;
            foreach (var prof in profs)
            {
                list_personnels.Items.Add(prof.Prenom + ' ' + prof.Nom);
            }
        }
    }
}
