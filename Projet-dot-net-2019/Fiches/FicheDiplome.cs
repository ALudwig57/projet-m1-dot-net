﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.ControlsForm
{
    /// <summary>
    /// Fiche permettant d'afficher les infos d'un diplôme lorsqu'on le selectionne
    /// </summary>
    public partial class FicheDiplome : UserControl
    {
        private Diplome diplome;

        /// <summary>
        /// Constructeur de la fiche
        /// </summary>
        /// <param name="id">Id du diplôme à afficher</param>
        /// <param name="service">Service du diplôme à afficher</param>
        public FicheDiplome(int id, DiplomeService service)
        {
            diplome = service.Get(id);
            InitializeComponent();
            l_nom_diplome.Text = diplome.Nom;
            foreach (AnneeUniv anneeUniv in diplome.AnneeUnivs)
            {
                list_annees.Items.Add(anneeUniv.year);
            }

        }

        private void FicheDiplome_Load(object sender, EventArgs e)
        {

        }
    }
}
