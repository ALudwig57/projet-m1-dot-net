﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Fiches
{
    /// <summary>
    /// Fiche de EC
    /// </summary>
    public partial class FicheEC : UserControl
    {
        private ECService ecService;
        private UEService ueService;
        private SeanceService seanceService;

        private EC ec;

        /// <summary>
        /// Constructeur de la fiche de l'EC
        /// </summary>
        /// <param name="id">Id de l'EC</param>
        /// <param name="ecService">Service de l'EC</param>
        /// <param name="ueService">Service de l'UE de l'EC</param>
        /// <param name="seanceService">Service des séances de l'EC </param>
        public FicheEC(int id, ECService ecService, UEService ueService, SeanceService seanceService)
        {
            this.ecService = ecService;
            this.ueService = ueService;
            this.seanceService = seanceService;
            InitializeComponent();
            ec = ecService.Get(id);
            l_nom_ec.Text = ec.Nom;
            l_ue.Text = ec.UE.Nom;
            FillCours();
            FillProfs();
        }

        private void FicheEC_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Remplie/Mets à jour la liste des cours de la fiche
        /// </summary>
        private void FillCours()
        {
            list_profs.Items.Clear();
            var cours = ec.Seances;
            foreach (var seance in cours)
            {
                list_cours.Items.Add(seance.TypeCours);
            }
        }

        /// <summary>
        /// Remplie/Mets à jour la liste des personnels de la fiche
        /// </summary>
        private void FillProfs()
        {
            list_profs.Items.Clear();
            var profs = ec.Personnels.Distinct();
            foreach (var prof in profs)
            {
                list_profs.Items.Add(prof.Prenom + ' ' + prof.Nom);
            }
        }
    }
}
