﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.ControlsForm
{
    /// <summary>
    /// Fiche affichant les informations d'une categorie
    /// </summary>
    public partial class FicheCategory : UserControl
    {
        //catégorie concernée
        private Categorie categorie;
        

        /// <summary>
        /// Constructeur de la fiche
        /// </summary>
        /// <param name="id">Id de la catégorie à afficher</param>
        /// <param name="service">Service de la catégorie</param>
        public FicheCategory(int id, CategorieService service)
        {
            categorie = service.Get(id);
            InitializeComponent();
            l_nom_cat.Text = categorie.Nom;
            l_nb_h.Text = categorie.NbHeures.ToString();
            l_eq_cm.Text = categorie.EqCM;
            l_eq_tp.Text = categorie.EqTP;
            l_eq_ei.Text = categorie.EqEI;
        }


        private void FicheCategory_Load(object sender, EventArgs e)
        {

        }
    }
}
