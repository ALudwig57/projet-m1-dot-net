﻿namespace Projet_dot_net_2019.Fiches
{
    partial class FicheSeance
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.ec = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.type = new System.Windows.Forms.Label();
            this.nbHeures = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.prof = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.nbGroupes = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ec
            // 
            this.ec.AutoSize = true;
            this.ec.Location = new System.Drawing.Point(246, 195);
            this.ec.Name = "ec";
            this.ec.Size = new System.Drawing.Size(19, 13);
            this.ec.TabIndex = 26;
            this.ec.Text = "ec";
            this.ec.Click += new System.EventHandler(this.ec_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 195);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "EC Concernée :";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // type
            // 
            this.type.AutoSize = true;
            this.type.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.type.Location = new System.Drawing.Point(31, 20);
            this.type.Name = "type";
            this.type.Size = new System.Drawing.Size(76, 25);
            this.type.TabIndex = 24;
            this.type.Text = "label1";
            // 
            // nbHeures
            // 
            this.nbHeures.AutoSize = true;
            this.nbHeures.Location = new System.Drawing.Point(246, 237);
            this.nbHeures.Name = "nbHeures";
            this.nbHeures.Size = new System.Drawing.Size(53, 13);
            this.nbHeures.TabIndex = 28;
            this.nbHeures.Text = "nbHeures";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(33, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Nb Heures :";
            // 
            // prof
            // 
            this.prof.AutoSize = true;
            this.prof.Location = new System.Drawing.Point(246, 315);
            this.prof.Name = "prof";
            this.prof.Size = new System.Drawing.Size(0, 13);
            this.prof.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(33, 315);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(78, 13);
            this.label5.TabIndex = 29;
            this.label5.Text = "Enseignant :";
            // 
            // nbGroupes
            // 
            this.nbGroupes.AutoSize = true;
            this.nbGroupes.Location = new System.Drawing.Point(246, 275);
            this.nbGroupes.Name = "nbGroupes";
            this.nbGroupes.Size = new System.Drawing.Size(59, 13);
            this.nbGroupes.TabIndex = 32;
            this.nbGroupes.Text = "nbGroupes";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(33, 275);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 31;
            this.label7.Text = "Nb groupes :";
            // 
            // FicheSeance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.nbGroupes);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.prof);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.nbHeures);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ec);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.type);
            this.Name = "FicheSeance";
            this.Size = new System.Drawing.Size(490, 448);
            this.Load += new System.EventHandler(this.FicheSeance_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ec;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label nbHeures;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label prof;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label nbGroupes;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label type;
    }
}
