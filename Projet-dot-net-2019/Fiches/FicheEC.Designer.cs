﻿namespace Projet_dot_net_2019.Fiches
{
    partial class FicheEC
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.list_cours = new System.Windows.Forms.ListBox();
            this.l_ue = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.l_nom_ec = new System.Windows.Forms.Label();
            this.list_profs = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // list_cours
            // 
            this.list_cours.FormattingEnabled = true;
            this.list_cours.Location = new System.Drawing.Point(242, 204);
            this.list_cours.Name = "list_cours";
            this.list_cours.Size = new System.Drawing.Size(214, 95);
            this.list_cours.TabIndex = 24;
            // 
            // l_ue
            // 
            this.l_ue.AutoSize = true;
            this.l_ue.Location = new System.Drawing.Point(239, 162);
            this.l_ue.Name = "l_ue";
            this.l_ue.Size = new System.Drawing.Size(35, 13);
            this.l_ue.TabIndex = 23;
            this.l_ue.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 162);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "UE concernée : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Types de cours :";
            // 
            // l_nom_ec
            // 
            this.l_nom_ec.AutoSize = true;
            this.l_nom_ec.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nom_ec.Location = new System.Drawing.Point(24, 16);
            this.l_nom_ec.Name = "l_nom_ec";
            this.l_nom_ec.Size = new System.Drawing.Size(76, 25);
            this.l_nom_ec.TabIndex = 20;
            this.l_nom_ec.Text = "label1";
            // 
            // list_profs
            // 
            this.list_profs.FormattingEnabled = true;
            this.list_profs.Location = new System.Drawing.Point(242, 305);
            this.list_profs.Name = "list_profs";
            this.list_profs.Size = new System.Drawing.Size(214, 95);
            this.list_profs.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 305);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Enseignants :";
            // 
            // FicheEC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.list_profs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.list_cours);
            this.Controls.Add(this.l_ue);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.l_nom_ec);
            this.Name = "FicheEC";
            this.Size = new System.Drawing.Size(475, 423);
            this.Load += new System.EventHandler(this.FicheEC_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox list_cours;
        private System.Windows.Forms.Label l_ue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label l_nom_ec;
        private System.Windows.Forms.ListBox list_profs;
        private System.Windows.Forms.Label label2;
    }
}
