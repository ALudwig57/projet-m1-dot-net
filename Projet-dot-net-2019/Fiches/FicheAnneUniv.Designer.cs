﻿namespace Projet_dot_net_2019.ControlsForm
{
    partial class FicheAnneUniv
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.l_nom_annee = new System.Windows.Forms.Label();
            this.list_ue = new System.Windows.Forms.ListBox();
            this.l_nb_h = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(33, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(191, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "UE appartenant à cette année : ";
            // 
            // l_nom_annee
            // 
            this.l_nom_annee.AutoSize = true;
            this.l_nom_annee.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nom_annee.Location = new System.Drawing.Point(31, 21);
            this.l_nom_annee.Name = "l_nom_annee";
            this.l_nom_annee.Size = new System.Drawing.Size(76, 25);
            this.l_nom_annee.TabIndex = 9;
            this.l_nom_annee.Text = "label1";
            // 
            // list_ue
            // 
            this.list_ue.FormattingEnabled = true;
            this.list_ue.Location = new System.Drawing.Point(230, 279);
            this.list_ue.Name = "list_ue";
            this.list_ue.Size = new System.Drawing.Size(223, 134);
            this.list_ue.TabIndex = 14;
            // 
            // l_nb_h
            // 
            this.l_nb_h.AutoSize = true;
            this.l_nb_h.Location = new System.Drawing.Point(228, 237);
            this.l_nb_h.Name = "l_nb_h";
            this.l_nb_h.Size = new System.Drawing.Size(35, 13);
            this.l_nb_h.TabIndex = 16;
            this.l_nb_h.Text = "label6";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(33, 237);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Nombre d\'heures cumulées : ";
            // 
            // FicheAnneUniv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.l_nb_h);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.list_ue);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.l_nom_annee);
            this.Name = "FicheAnneUniv";
            this.Size = new System.Drawing.Size(466, 426);
            this.Load += new System.EventHandler(this.FicheAnneUniv_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label l_nom_annee;
        private System.Windows.Forms.ListBox list_ue;
        private System.Windows.Forms.Label l_nb_h;
        private System.Windows.Forms.Label label1;
    }
}
