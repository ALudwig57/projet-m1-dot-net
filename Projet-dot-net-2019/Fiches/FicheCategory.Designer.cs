﻿namespace Projet_dot_net_2019.ControlsForm
{
    partial class FicheCategory
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_nom_cat = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.l_nb_h = new System.Windows.Forms.Label();
            this.l_eq_tp = new System.Windows.Forms.Label();
            this.l_eq_ei = new System.Windows.Forms.Label();
            this.l_eq_cm = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // l_nom_cat
            // 
            this.l_nom_cat.AutoSize = true;
            this.l_nom_cat.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nom_cat.Location = new System.Drawing.Point(34, 28);
            this.l_nom_cat.Name = "l_nom_cat";
            this.l_nom_cat.Size = new System.Drawing.Size(76, 25);
            this.l_nom_cat.TabIndex = 0;
            this.l_nom_cat.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 319);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Equivalent CM pour 1 TD : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 351);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Equivalent TP pour 1 TD : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 385);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Equivalent EI pour 1 TD : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(180, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nombre d\'heures à effectuer : ";
            // 
            // l_nb_h
            // 
            this.l_nb_h.AutoSize = true;
            this.l_nb_h.Location = new System.Drawing.Point(231, 286);
            this.l_nb_h.Name = "l_nb_h";
            this.l_nb_h.Size = new System.Drawing.Size(35, 13);
            this.l_nb_h.TabIndex = 5;
            this.l_nb_h.Text = "label6";
            // 
            // l_eq_tp
            // 
            this.l_eq_tp.AutoSize = true;
            this.l_eq_tp.Location = new System.Drawing.Point(231, 351);
            this.l_eq_tp.Name = "l_eq_tp";
            this.l_eq_tp.Size = new System.Drawing.Size(35, 13);
            this.l_eq_tp.TabIndex = 6;
            this.l_eq_tp.Text = "label7";
            // 
            // l_eq_ei
            // 
            this.l_eq_ei.AutoSize = true;
            this.l_eq_ei.Location = new System.Drawing.Point(231, 385);
            this.l_eq_ei.Name = "l_eq_ei";
            this.l_eq_ei.Size = new System.Drawing.Size(35, 13);
            this.l_eq_ei.TabIndex = 7;
            this.l_eq_ei.Text = "label8";
            // 
            // l_eq_cm
            // 
            this.l_eq_cm.AutoSize = true;
            this.l_eq_cm.Location = new System.Drawing.Point(231, 319);
            this.l_eq_cm.Name = "l_eq_cm";
            this.l_eq_cm.Size = new System.Drawing.Size(35, 13);
            this.l_eq_cm.TabIndex = 8;
            this.l_eq_cm.Text = "label9";
            // 
            // FicheCategory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.l_eq_cm);
            this.Controls.Add(this.l_eq_ei);
            this.Controls.Add(this.l_eq_tp);
            this.Controls.Add(this.l_nb_h);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.l_nom_cat);
            this.Name = "FicheCategory";
            this.Size = new System.Drawing.Size(466, 426);
            this.Load += new System.EventHandler(this.FicheCategory_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_nom_cat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label l_nb_h;
        private System.Windows.Forms.Label l_eq_tp;
        private System.Windows.Forms.Label l_eq_ei;
        private System.Windows.Forms.Label l_eq_cm;
    }
}
