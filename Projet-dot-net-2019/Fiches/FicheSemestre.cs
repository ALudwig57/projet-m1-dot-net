﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.ControlsForm
{
    /// <summary>
    /// Fiche permettant d'afficher les informations d'un semestre
    /// </summary>
    public partial class FicheSemestre : UserControl
    {
        //Semestre concerné
        private Semestre semestre;
        
        /// <summary>
        /// Constructeur de la fiche
        /// </summary>
        /// <param name="id">Id du semestre concerné</param>
        /// <param name="service">Service du semestre</param>
        public FicheSemestre(int id, SemestreService service)
        {
            semestre = service.Get(id);
            InitializeComponent();
            l_nom_semestre.Text = semestre.name;
            int nbheures = 0;
            foreach (UE ue in semestre.UEs)
            {
                listBox_UEs_Semestre.Items.Add(ue.Nom);
                nbheures = ue.CumulHeures;
            }

            l_heures_total.Text = nbheures.ToString() + " h";

        }

        private void FicheSemestre_Load(object sender, EventArgs e)
        {

        }
    }
}
