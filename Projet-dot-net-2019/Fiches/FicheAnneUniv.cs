﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.ControlsForm
{
    /// <summary>
    /// Fiche d'une année universitaire
    /// </summary>
    public partial class FicheAnneUniv : UserControl
    {
        private DiplomeService diplomeService;

        /// <summary>
        /// Constructeur de la fiche
        /// </summary>
        /// <param name="id">Id de l'année universitaire</param>
        /// <param name="anneeUnivService">Service pour accéder aux fonctions CRUD de l'année universitaire</param>
        /// <param name="diplomeService">Service pour accéder aux infos liées au diplome</param>
        public FicheAnneUniv(int id, AnneeUnivService anneeUnivService, DiplomeService diplomeService)
        {
            AnneeUniv anneeUniv = anneeUnivService.Get(id);
            Diplome diplome = diplomeService.GetDiplomeFromAnnee(anneeUniv);
            InitializeComponent();
            l_nom_annee.Text = diplome.Nom + " " + anneeUniv.year;
            FillListUE(anneeUniv);
            l_nb_h.Text = getCumulHeures(anneeUniv).ToString() + " heures";

        }

        private void FicheAnneUniv_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Calcule le cumul d'heures d'une année universitaire en calculant la somme des heures des UE du premier et seconde semestre
        /// </summary>
        /// <param name="anneeUniv">Annee Universitaire concernée</param>
        /// <returns>Le nombre d'heures total(double)</returns>
        private int getCumulHeures(AnneeUniv anneeUniv)
        {
            int nbh = 0;
            foreach (UE ue in anneeUniv.S1.UEs)
            {
                nbh += ue.CumulHeures;

            }

            foreach (UE ue in anneeUniv.S2.UEs)
            {
                nbh += ue.CumulHeures;

            }

            return nbh;

        }

        /// <summary>
        /// Remplie/met à jour la liste des UEs de cette fiche avec les UE associées à l'année
        /// </summary>
        /// <param name="anneeUniv">Année concernée</param>
        private void FillListUE(AnneeUniv anneeUniv)
        {
            //Réinitialisation
            list_ue.Items.Clear();
            foreach (UE ue in anneeUniv.S1.UEs)
            {
                list_ue.Items.Add(ue.Nom);
            }

            foreach (UE ue in anneeUniv.S2.UEs)
            {
                list_ue.Items.Add(ue.Nom);
            }
        }
    }
}
