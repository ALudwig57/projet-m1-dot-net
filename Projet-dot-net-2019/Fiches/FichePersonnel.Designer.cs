﻿namespace Projet_dot_net_2019.ControlsForm
{
    partial class FichePersonnel
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_category = new System.Windows.Forms.Label();
            this.l_nb_h = new System.Windows.Forms.Label();
            this.l_email = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.l_nom_pers = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // l_category
            // 
            this.l_category.AutoSize = true;
            this.l_category.Location = new System.Drawing.Point(227, 335);
            this.l_category.Name = "l_category";
            this.l_category.Size = new System.Drawing.Size(35, 13);
            this.l_category.TabIndex = 17;
            this.l_category.Text = "label9";
            // 
            // l_nb_h
            // 
            this.l_nb_h.AutoSize = true;
            this.l_nb_h.Location = new System.Drawing.Point(227, 367);
            this.l_nb_h.Name = "l_nb_h";
            this.l_nb_h.Size = new System.Drawing.Size(35, 13);
            this.l_nb_h.TabIndex = 15;
            this.l_nb_h.Text = "label7";
            this.l_nb_h.Click += new System.EventHandler(this.l_nb_h_Click);
            // 
            // l_email
            // 
            this.l_email.AutoSize = true;
            this.l_email.Location = new System.Drawing.Point(227, 302);
            this.l_email.Name = "l_email";
            this.l_email.Size = new System.Drawing.Size(35, 13);
            this.l_email.TabIndex = 14;
            this.l_email.Text = "label6";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 302);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Email : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(32, 367);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Nb heures / à effectuer : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(32, 335);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Catégorie : ";
            // 
            // l_nom_pers
            // 
            this.l_nom_pers.AutoSize = true;
            this.l_nom_pers.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l_nom_pers.Location = new System.Drawing.Point(30, 31);
            this.l_nom_pers.Name = "l_nom_pers";
            this.l_nom_pers.Size = new System.Drawing.Size(76, 25);
            this.l_nom_pers.TabIndex = 9;
            this.l_nom_pers.Text = "label1";
            // 
            // FichePersonnel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.l_category);
            this.Controls.Add(this.l_nb_h);
            this.Controls.Add(this.l_email);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.l_nom_pers);
            this.Name = "FichePersonnel";
            this.Size = new System.Drawing.Size(466, 426);
            this.Load += new System.EventHandler(this.FichePersonnel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_category;
        private System.Windows.Forms.Label l_nb_h;
        private System.Windows.Forms.Label l_email;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label l_nom_pers;
    }
}
