namespace Projet_dot_net_2019.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnneeUnivs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        year = c.String(),
                        S1ID = c.Int(nullable: false),
                        S2ID = c.Int(nullable: false),
                        AnneeDiplomeID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Diplomes", t => t.AnneeDiplomeID)
                .ForeignKey("dbo.Semestres", t => t.S1ID, cascadeDelete: false)
                .ForeignKey("dbo.Semestres", t => t.S2ID, cascadeDelete: false)
                .Index(t => t.S1ID)
                .Index(t => t.S2ID)
                .Index(t => t.AnneeDiplomeID);
            
            CreateTable(
                "dbo.Diplomes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Semestres",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UEs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                        SemestreUEID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Semestres", t => t.SemestreUEID, cascadeDelete: true)
                .Index(t => t.SemestreUEID);
            
            CreateTable(
                "dbo.ECs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                        UE_ECID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UEs", t => t.UE_ECID)
                .Index(t => t.UE_ECID);
            
            CreateTable(
                "dbo.Personnels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoriePersonnelID = c.Int(nullable: false),
                        Nom = c.String(),
                        Prenom = c.String(),
                        Email = c.String(),
                        NbHeures = c.Int(nullable: false),
                        EC_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoriePersonnelID, cascadeDelete: true)
                .ForeignKey("dbo.ECs", t => t.EC_ID)
                .Index(t => t.CategoriePersonnelID)
                .Index(t => t.EC_ID);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                        NbHeures = c.Int(nullable: false),
                        EqCM = c.String(),
                        EqTP = c.String(),
                        EqEI = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Seances",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NbHeures = c.Int(nullable: false),
                        SeanceECID = c.Int(),
                        PersonnelSeanceID = c.Int(),
                        NbGroupes = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.ECs", t => t.SeanceECID)
                .ForeignKey("dbo.Personnels", t => t.PersonnelSeanceID)
                .Index(t => t.SeanceECID)
                .Index(t => t.PersonnelSeanceID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AnneeUnivs", "S2ID", "dbo.Semestres");
            DropForeignKey("dbo.AnneeUnivs", "S1ID", "dbo.Semestres");
            DropForeignKey("dbo.UEs", "SemestreUEID", "dbo.Semestres");
            DropForeignKey("dbo.ECs", "UE_ECID", "dbo.UEs");
            DropForeignKey("dbo.Personnels", "EC_ID", "dbo.ECs");
            DropForeignKey("dbo.Seances", "PersonnelSeanceID", "dbo.Personnels");
            DropForeignKey("dbo.Seances", "SeanceECID", "dbo.ECs");
            DropForeignKey("dbo.Personnels", "CategoriePersonnelID", "dbo.Categories");
            DropForeignKey("dbo.AnneeUnivs", "AnneeDiplomeID", "dbo.Diplomes");
            DropIndex("dbo.Seances", new[] { "PersonnelSeanceID" });
            DropIndex("dbo.Seances", new[] { "SeanceECID" });
            DropIndex("dbo.Personnels", new[] { "EC_ID" });
            DropIndex("dbo.Personnels", new[] { "CategoriePersonnelID" });
            DropIndex("dbo.ECs", new[] { "UE_ECID" });
            DropIndex("dbo.UEs", new[] { "SemestreUEID" });
            DropIndex("dbo.AnneeUnivs", new[] { "AnneeDiplomeID" });
            DropIndex("dbo.AnneeUnivs", new[] { "S2ID" });
            DropIndex("dbo.AnneeUnivs", new[] { "S1ID" });
            DropTable("dbo.Seances");
            DropTable("dbo.Categories");
            DropTable("dbo.Personnels");
            DropTable("dbo.ECs");
            DropTable("dbo.UEs");
            DropTable("dbo.Semestres");
            DropTable("dbo.Diplomes");
            DropTable("dbo.AnneeUnivs");
        }
    }
}
