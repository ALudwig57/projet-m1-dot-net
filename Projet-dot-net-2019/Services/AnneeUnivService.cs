﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;

namespace Projet_dot_net_2019.Services
{
    public class AnneeUnivService: Service<AnneeUniv>, IService
    {
        /// <summary>
        /// Service du semestre qui lui est affecté
        /// </summary>
        private SemestreService _semestreService;

        public SemestreService SemestreService
        {
            set => _semestreService = value;
        }

        public AnneeUnivService(Repository<AnneeUniv> repository) : base(repository)
        {
        }

        /// <summary>
        /// Méthode de suppression récursive : On supprime d'abord l'année puis les semestres
        /// </summary>
        /// <param name="anneeUniv"></param>
        public override void Delete(AnneeUniv anneeUniv)
        {
            Semestre S1 = anneeUniv.S1, S2 = anneeUniv.S2;
            this.repository.Delete(anneeUniv);
            this.repository.Complete();
            this._semestreService.Delete(S1);
            this._semestreService.Delete(S2);
            this.repository.Complete();
        }

        
        public override AnneeUniv Get(int id)
        {
            return this.repository.Get(id);
        }

        public override void Delete(int id)
        {
            this.repository.Delete(this.repository.Get(id));
            this.repository.Complete();
        }

        /// <summary>
        /// Ajout d'année universitaire et des semestres
        /// </summary>
        /// <param name="year">Nom de l'année (L1, L2, L3...)</param>
        public void addAnneeUniversitaire(string year)
        {
                var anneeUniversitaire = new AnneeUniv();
                var S1 = new Semestre();
                var S2 = new Semestre();

                S1.name = "Premier semestre de l'année " + year;
                S2.name = "Deuxième semestre de l'année " + year;

                anneeUniversitaire.year = year;
                anneeUniversitaire.S1 = S1;
                anneeUniversitaire.S2 = S2;
                ///Ajout dans la base
                this.repository.Add(anneeUniversitaire);
                ///Sauvegarde des changements dans la base
                this.repository.Complete();
        }

        public void deleteAnneeUniversitaire(AnneeUniv anneeUniv)
        {
            
        }
        
        /// <summary>
        /// Mise à jout d'une année universitaire
        /// </summary>
        /// <param name="id">Id de l'année à modifier</param>
        /// <param name="year">Nom de l'année</param>
        public void updateAnneeUniversitaire(int id, string year)
        {
            var anneeUniversitaire = this.repository.Get(id);
            if (anneeUniversitaire != null)
            {
                anneeUniversitaire.year = year;
            }
            this.repository.Complete();
        }

        /// <summary>
        /// Récupère toutes les années universitaires de la base
        /// </summary>
        /// <returns>Liste des années universitaires</returns>
        public IEnumerable<AnneeUniv> GetAllAnneesUniv()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<AnneeUniv> FindAnnee(Expression<Func<AnneeUniv, bool>> predicate)
        {
            return this.repository.Find(predicate);
        }

        /// <summary>
        /// Récupère les années d'un diplome
        /// </summary>
        /// <param name="id">id du diplôme</param>
        /// <returns>liste des années du diplôme</returns>
        public IEnumerable<AnneeUniv> GetAnneeUnivsFromDiplome(int id)
        {
            return this.repository.Find(x => x.Diplome.ID == id);
        }

        /// <summary>
        /// Récupère les années qui ne sont pas affectées à un diplôme
        /// </summary>
        /// <returns></returns>
        public IEnumerable<AnneeUniv> GetAnneesOrphelines()
        {
            return this.repository.Find(x => x.Diplome.ID == null);
        }

        public Semestre GetS1(int id)
        {
            return _semestreService.Get(this.repository.Get(id).S1.ID);
        }

        public Semestre GetS2(int id)
        {
            return _semestreService.Get(this.repository.Get(id).S2.ID);
        }

        public void setModified(AnneeUniv annee)
        {
            this.repository.Update(annee);
        }

        /// <summary>
        /// Récupère l'année d'un semestre
        /// </summary>
        /// <param name="semestre"></param>
        /// <returns></returns>
        public AnneeUniv GetAnneeOfSemestre(Semestre semestre)
        {
            IEnumerable<AnneeUniv> annees = this.repository.GetAll();
            foreach (var annee in annees)
            {
                if (annee.S1.ID == semestre.ID || annee.S2.ID == semestre.ID)
                    return annee;
            }

            return null;
        }
    }
}
