﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;

namespace Projet_dot_net_2019.Services
{
    public class UEService: Service<UE>, IService
    {
        private SemestreService _semestreService;
        private ECService _ecService;

        public SemestreService SemestreService
        {
            set => _semestreService = value;
        }

        public ECService ECService
        {
            set => _ecService = value;
        }

        public UEService(Repository<UE> repository) : base(repository)
        {
        }

        public override UE Get(int id)
        {
            return this.repository.Get(id);
        }

        public override void Delete(UE ue)
        {
            IList<EC> ECs = (IList<EC>)ue.ECs;
            for (int i = ECs.Count - 1; i >= 0; i--)
            {
                this._ecService.Delete(ECs[i]);
            }

            this.repository.Complete();
            this.repository.Delete(ue);
            this.repository.Complete();
        }

        public override void Delete(int id)
        {
            this.repository.Delete(this.repository.Get(id));
            this.repository.Complete();
        }

        public void Add(UE ue)
        {
            this.repository.Add(ue);
            this.repository.Complete();
        }

        public void Add(string nom, List<EC> ecs, int idSemestre)
        {
            var ue = new UE
            {
                Nom = nom,
            };
            foreach (EC ec in ecs)
            {
                this.repository.Context.ec.Attach(ec);
                ue.ECs.Add(ec);
            }
            this.repository.Add(ue);
            Semestre semestre = _semestreService.Get(idSemestre);
            semestre.UEs.Add(ue);
            this.repository.Complete();
        }

        public void Update(int id, string nom, List<EC> ecs, int idSemestre)
        {
            var ue = this.repository.Get(id);
            ue.Nom = nom;
            ue.ECs = ecs;
            ue.Semestre = _semestreService.Get(idSemestre);
            this.repository.Complete();
        }

        public IEnumerable<EC> GetECs(int id)
        {
            return this.repository.Get(id).ECs;
        }

        public IEnumerable<UE> GetAllUes()
        {
            return this.repository.GetAll();
        }
    }
}
