﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Services
{
    public class PersonnelService: Service<Personnel>, IService
    {
        private CategorieService _categorieService;
        private SeanceService _seanceService;

        public CategorieService CategorieService
        {
            set => _categorieService = value;
        }

        public SeanceService SeanceService
        {
            set => _seanceService = value;
        }

        public PersonnelService(Repository<Personnel> repository) : base(repository)
        {
        }

        public override void Delete(Personnel personnel)
        {
            foreach (var seance in personnel.Seances)
            {
                this._seanceService.Delete(seance);
            }
            this.repository.Complete();
            this.repository.Delete(personnel);
            this.repository.Complete();
        }

        public override Personnel Get(int id)
        {
            return this.repository.Get(id);
        }

        public override void Delete(int id)
        {
            this.repository.Delete(this.repository.Get(id));
            this.repository.Complete();
        }

        /**
         * Ajout d'un personnel dans la BDD
         */
        public void addPersonnel(string name, string forname, string email, int nbHeures, int CategorieID)
        {
            using (CategorieRepository categoryRepository = new CategorieRepository(new ApplicationDbContext()))
            {
                var personnel = new Personnel();
                personnel.Prenom = forname;
                personnel.Nom = name;
                personnel.NbHeures = nbHeures;
                personnel.Email = email;
                personnel.Categorie = _categorieService.Get(CategorieID);
                this.repository.Add(personnel);
                this.repository.Complete();
            }
        }

        public void updatePersonnel(int id, string name, string forname, string email, int nbHeures, int CategorieID )
        {
            Personnel personnel = this.repository.Get(id);
            if (personnel != null)
            {
                personnel.Nom = name;
                personnel.Prenom = forname;
                personnel.Email = email;
                personnel.NbHeures = nbHeures;
                personnel.Categorie = _categorieService.Get(CategorieID);
                this.repository.Complete();
            }
        }

        public IEnumerable<Personnel> GetAllPersonnels()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<Personnel> GetPersonnelsFromCategory(Categorie categorie)
        {
            return this.repository.Find(x => x.Categorie.ID == categorie.ID);
        }

        public IEnumerable<Personnel> FindPersonnels(Expression<Func<Personnel, bool>> predicate)
        {
            return this.repository.Find(predicate);
        }

        public Categorie GetCategorieOfPersonnel(int id)
        {
            return _categorieService.Get(Get(id).Categorie.ID);
        }

    }

}
