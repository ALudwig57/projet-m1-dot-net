﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;

namespace Projet_dot_net_2019.Services
{
    public class SeanceService: Service<Seance>, IService
    {
        public SeanceService(Repository<Seance> repository) : base(repository)
        {
        }

        public override Seance Get(int id)
        {
            return this.repository.Get(id);
        }

        public override void Delete(Seance seance)
        {
            this.repository.Delete(seance);
            this.repository.Complete();
        }

        public override void Delete(int id)
        {
            this.repository.Delete(this.repository.Get(id));
            this.repository.Complete();
        }

        public void setPersonnel(int SeanceID, Personnel personnel)
        {
            this.repository.Get(SeanceID).Personnel = personnel;
            this.repository.Complete();
        }

        public void AddCM(int nbHeures, Personnel personnel, int ECID)
        {
            CM cm = new CM
            {
                NbHeures = nbHeures,
                Personnel = personnel,
                SeanceECID = ECID
            };
            this.repository.Add(cm);
            this.repository.Complete();
        }


        public void AddTP(int nbHeures, int nbGroupes, Personnel personnel, int ECID)
        {
            TP tp = new TP
            {
                NbHeures = nbHeures,
                Personnel = personnel,
                NbGroupes = nbGroupes,
                SeanceECID = ECID
            };
            this.repository.Add(tp);
            this.repository.Complete();
        }

        public void AddTD(int nbHeures, int nbGroupes, Personnel personnel, int ECID)
        {
            TD td = new TD
            {
                NbHeures = nbHeures,
                Personnel = personnel,
                NbGroupes = nbGroupes,
                SeanceECID = ECID
            };
            this.repository.Add(td);
            this.repository.Complete();
        }


        public void AddEI(int nbHeures, int nbGroupes, Personnel personnel, int ECID)
        {
            EI ei = new EI
            {
                NbHeures = nbHeures,
                Personnel = personnel,
                NbGroupes = nbGroupes,
                SeanceECID = ECID
            };
            this.repository.Add(ei);
            this.repository.Complete();
        }

        public void Add(Seance seance)
        {
            this.repository.Add(seance);
            this.repository.Complete();
        }

        public IEnumerable<Seance> GetSeances()
        {
            return this.repository.GetAll();
        }
    }
}
