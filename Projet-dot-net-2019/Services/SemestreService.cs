﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;

namespace Projet_dot_net_2019.Services
{
    public class SemestreService:Service<Semestre>, IService
    {
        private UEService _ueService;

        public UEService UEService
        {
            set => this._ueService = value;
        }

        public SemestreService(Repository<Semestre> repository) : base(repository)
        {
        }

        public override Semestre Get(int id)
        {
            return this.repository.Get(id);
        }

        /// <summary>
        /// Suppression d'un semestre puis des UE qui lui sont affecté
        /// </summary>
        /// <param name="semestre"></param>
        public override void Delete(Semestre semestre)
        {
            IList<UE> UEs = (IList<UE>)semestre.UEs;
            for (int i = UEs.Count - 1; i >= 0; i--)
            {
                this._ueService.Delete(UEs[i]);
            }

            this.repository.Complete();
            this.repository.Delete(semestre);
            this.repository.Complete();
        }

        public override void Delete(int id)
        {
            this.repository.Delete(this.repository.Get(id));
            this.repository.Complete();
        }

        public IEnumerable<UE> GetUESOfSemestre(int id)
        {
            return this.repository.Get(id).UEs;
        }

        public IEnumerable<Semestre> GetAllSemestres()
        {
            return this.repository.GetAll();
        }

        public void DeleteSemestresOfAnneeUniv(AnneeUniv annee)
        {
            this.repository.Delete(this.repository.Get(annee.S1ID));
            this.repository.Delete(this.repository.Get(annee.S2ID));
            this.repository.Complete();
        }

        public void DeleteSemestre(Semestre semestre)
        {
            this.repository.Delete(semestre);
            this.repository.Complete();
        }

        public void DeleteSemestre(int id)
        {
            this.repository.Delete(this.repository.Get(id));
            this.repository.Complete();
        }

        public void UpdateSemestre(Semestre semestre)
        {
            this.repository.Update(semestre);
            this.repository.Complete();
        }
    }
}
