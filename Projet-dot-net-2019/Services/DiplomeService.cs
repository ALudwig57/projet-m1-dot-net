﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;
using Projet_dot_net_2019.Services;

namespace Projet_dot_net_2019.Services
{
    public class DiplomeService:Service<Diplome>, IService
    {
        private AnneeUnivService _anneeUnivService;

        public AnneeUnivService AnneeUnivService
        {
            set => this._anneeUnivService = value;
        }

        public DiplomeService(Repository<Diplome> repository) : base(repository)
        {
        }

        public override void Delete(Diplome diplome)
        {
            this.repository.Delete(diplome);
        }

        public override Diplome Get(int id)
        {
            return this.repository.Get(id);
        }

        /// <summary>
        /// Suppression des années universitaires puis du diplôme
        /// </summary>
        /// <param name="id"></param>
        public override void Delete(int id)
        {
            Diplome diplome = this.repository.Get(id);
            IList<AnneeUniv> anneeUnivs = (IList<AnneeUniv>)diplome.AnneeUnivs;
            for (int i = diplome.AnneeUnivs.Count - 1; i >= 0; i--)
            {
                this._anneeUnivService.Delete(anneeUnivs[i]);
            }

            this.repository.Complete();
            this.repository.Delete(diplome);
            this.repository.Complete();
        }

        public IEnumerable<Diplome> GetAllDiplomes()
        {
            return this.repository.GetAll();
        }

        public void SetDiplome(string name, List<AnneeUniv> listeAnnees)
        {

        }

        /// <summary>
        /// Ajout d'un diplôme et attachement de ses années
        /// </summary>
        /// <param name="name"></param>
        /// <param name="listeAnnees"></param>
        public void AddDiplome(string name, List<AnneeUniv> listeAnnees)
        {
            var diplome = new Diplome();
            diplome.Nom = name;

            foreach (AnneeUniv anneeUniv in listeAnnees)
            {
                this.repository.Context.anneeUniv.Attach(anneeUniv);
                diplome.AnneeUnivs.Add(anneeUniv);
            }
            this.repository.Add(diplome);
            this.repository.Complete();
        }

        /// <summary>
        /// Mise à joure du nom et des années du diplôme
        /// </summary>
        /// <param name="Id">Id du diplôme à modifier</param>
        /// <param name="nom">Nom du diplôme</param>
        /// <param name="listeAnnees">Liste des années à affecter</param>
        public void updateDiplome(int Id, string nom, List<AnneeUniv> listeAnnees)
        {
            using (AnneeUnivRepository anneeRepository = new AnneeUnivRepository(new ApplicationDbContext()))
            {
                Diplome diplome = this.repository.Get(Id);

                if (diplome != null)
                {
                    diplome.Nom = nom;
                    diplome.AnneeUnivs = listeAnnees;
                    this.repository.Complete();
                }
            }
        }

        /// <summary>
        /// Récupère le dilôme d'une année
        /// </summary>
        /// <param name="anneeUniv"></param>
        /// <returns></returns>
        public Diplome GetDiplomeFromAnnee(AnneeUniv anneeUniv)
        {
            return this.repository.Find(x => x.ID == anneeUniv.Diplome.ID).First();
        }




    }
}
