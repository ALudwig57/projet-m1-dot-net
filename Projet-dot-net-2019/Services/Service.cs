﻿using System.Collections.Generic;
using Projet_dot_net_2019.Model;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Projet_dot_net_2019.Repositories;

namespace Projet_dot_net_2019.Services
{
    /// <summary>
    /// Service permettant de faire quelques traitements avant d'appeler les méthodes du repository
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class Service<TEntity> where TEntity : class
    {
        protected Repository<TEntity> repository;

        protected Service(Repository<TEntity> repository)
        {
           this.repository = repository;
        }


        /// <summary>
        /// Permet de supprimer une entrée dans la base à partir de son instance
        /// </summary>
        /// <param name="entity"></param>
        public abstract void Delete(TEntity entity);

        /// <summary>
        /// Permet de supprimer une entrée dans la base à partir de son id
        /// </summary>
        /// <param name="entity"></param>
        public abstract void Delete(int id);

        /// <summary>
        /// Permet de récupérer une entrée dans la base depuis son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public abstract TEntity Get(int id);

        /// <summary>
        /// Retourne le repository
        /// </summary>
        /// <returns></returns>
        public Repository<TEntity> GetRepository()
        {
            return this.repository;
        }
    }
}