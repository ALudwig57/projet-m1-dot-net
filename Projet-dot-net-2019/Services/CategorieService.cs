﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Windows.Forms;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;


namespace Projet_dot_net_2019.Services
{
    public class CategorieService: Service<Categorie>, IService
    {
        private PersonnelService _personnelService;

        public PersonnelService PersonnelService
        {
            set => _personnelService = value;
        }

        public CategorieService(Repository<Categorie> repository) : base(repository)
        {
        }

        public override void Delete(Categorie categorie)
        {
            this.repository.Delete(categorie);
            this.repository.Complete();
        }

        public override Categorie Get(int id)
        {
            return this.repository.Get(id);
        }

        /// <summary>
        /// Suppression d'une catégorie : On supprime d'abord les personnels associés, puis la catégorie
        /// </summary>
        /// <param name="id">ID de la catégorie à supprimer</param>
        public override void Delete(int id)
        {
            Categorie categorie = this.repository.Get(id);
            IList<Personnel> personnels = (IList<Personnel>)categorie.Personnels;
            for (int i = personnels.Count - 1; i >= 0; i--)
            {
                this._personnelService.Delete(personnels[i]);
            }
            this.repository.Complete();
            this.repository.Delete(categorie);
            this.repository.Complete();
        }

        /// <summary>
        ///     Retourne les personnels appartenant à une catégorie spécifique
        /// </summary>
        /// <param name="categorie">
        ///     Catégorie concernée
        /// </param>
        /// <returns>
        ///     Liste des personnels apparetenant à la catégorie.
        /// </returns>
        public IEnumerable<Personnel> GetPersonnelFromCategory(Categorie categorie)
        {
            using (PersonnelRepository personnelRepository = new PersonnelRepository(new ApplicationDbContext()))
            {
                return personnelRepository.Find(c => c.Categorie == categorie);
            }
        }

        /// <summary>
        ///     Vérifie les champs remplis dans le formulaire d'ajout de catégorie, puis enregistre la catégorie dans la BDD
        ///     si tout est OK.
        /// </summary>
        /// <param name="nom">      Nom de la catégorie</param>
        /// <param name="nbHeures"> Nombre d'heures que doit effectuer un personnel appartenant à cette catégorie</param>
        /// <param name="eqCM">     Equivalent CM pour un TD</param>
        /// <param name="eqTP">     Equivalent TP pour un TD</param>
        /// <param name="eqEI">     Equivalent EI pour un TD</param>
        public void addCategorie(string nom, int nbHeures, string eqCM, string eqTP, string eqEI)
        {
            var categorie = new Categorie();
            categorie.Nom = nom;
            categorie.NbHeures = nbHeures;
            categorie.EqCM = eqCM;
            categorie.EqTP = eqTP;
            categorie.EqEI = eqEI;
            this.repository.Add(categorie);
            this.repository.Complete();
        }

        public IEnumerable<Categorie> GetAllCategories()
        {
            return this.repository.GetAll();
        }


        public IEnumerable<Categorie> FindCategories(Expression<Func<Categorie,bool>> predicate)
        {
            return this.repository.Find(predicate);
        }
        /// <summary>
        ///     Met à jour la catégorie avec les paramètres saisis dans le formulaire
        /// </summary>
        /// <param name="id">      id de la catégorie concernée</param>
        /// <param name="nom">     identique à l'ajout</param>
        /// <param name="nbHeures">identique à l'ajout</param>
        /// <param name="eqCM">    identique à l'ajout</param>
        /// <param name="eqTP">    identique à l'ajout</param>
        /// <param name="eqEI">    identique à l'ajout</param>
        ///
        public void updateCategorie(int id, string nom, int nbHeures, string eqCM, string eqTP, string eqEI)
        {
            Categorie categorie = this.repository.Get(id);
            if (categorie != null)
            {
                categorie.Nom = nom;
                categorie.NbHeures = nbHeures;
                categorie.EqCM = eqCM;
                categorie.EqTP = eqTP;
                categorie.EqEI = eqEI;
                this.repository.Complete();
            }
        }

        public void setEqCM(Categorie categorie, string value)
        {
            this.repository.Get(categorie.ID).EqCM = value;
            this.repository.Complete();
        }

        public void setEqTP(Categorie categorie, string value)
        {
            this.repository.Get(categorie.ID).EqTP = value;
            this.repository.Complete();
        }

        public void setEqEI(Categorie categorie, string value)
        {
            this.repository.Get(categorie.ID).EqEI = value;
            this.repository.Complete();
        }

    }
}
