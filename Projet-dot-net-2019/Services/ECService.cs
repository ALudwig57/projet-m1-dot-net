﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Projet_dot_net_2019.Model;
using Projet_dot_net_2019.Repositories;

namespace Projet_dot_net_2019.Services
{
    public class ECService: Service<EC>, IService
    {
        private UEService _ueService;
        private SeanceService _seanceService;

        public SeanceService SeanceService
        {
            set => _seanceService = value;
        }

        public UEService UEService
        {
            set => _ueService = value;
        }

        public ECService(Repository<EC> repository) : base(repository)
        {
        }

        public override EC Get(int id)
        {
            return this.repository.Get(id);
        }

        /// <summary>
        /// Suppression des séances de l'EC puis de l'EC
        /// </summary>
        /// <param name="ec">CE à supprimer</param>
        public override void Delete(EC ec)
        {
            IList<Seance> Seances = (IList<Seance>)ec.Seances;
            for (int i = Seances.Count - 1; i >= 0; i--)
            {
                this._seanceService.Delete(Seances[i]);
            }

            this.repository.Complete();
            this.repository.Delete(ec);
            this.repository.Complete();
        }

        /// <summary>
        /// Paril qu'au dessus, sauf que l'on cherche un EC depuis son ID
        /// </summary>
        /// <param name="id"></param>
        public override void Delete(int id)
        {
            using (SeanceRepository seanceRepository = new SeanceRepository(new ApplicationDbContext()))
            {
                EC ec = this.repository.Get(id);

                foreach (var seance in ec.Seances)
                {
                    seanceRepository.Delete(seance);
                }

                seanceRepository.Complete();
                
                this.repository.Delete(ec);
                this.repository.Complete();
            }
        }

        /// <summary>
        /// Récupère la liste des enseignants d'une EC
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IEnumerable<Personnel> GetEnseignants(int id)
        {
            return this.repository.Get(id).Personnels;
        }

        /// <summary>
        /// Récupère les séances d'une EC 
        /// </summary>
        /// <param name="id">Id de l'EC</param>
        /// <returns></returns>
        public IEnumerable<Seance> GetSeances(int id)
        {
            return this.repository.Get(id).Seances;
        }

        public void Add(string nom)
        {
            var ec = new EC
            {
                Nom = nom,
                Seances = new List<Seance>(),
                Personnels = new List<Personnel>()
            };
            this.repository.Add(ec);
            this.repository.Complete();
        }

        public void Update(EC ec)
        {
            this.repository.Update(ec);
            this.repository.Complete();
        }

        public void UpdateEC(int id, string nom, List<Personnel> personnels, int idUe)
        {
            var ec = this.repository.Get(id);
            ec.Nom = nom;
            ec.Personnels = personnels;
            UE ue = _ueService.Get(idUe);
            ue.ECs.Add(ec);
            this.repository.Complete();
        }

        public IEnumerable<EC> GetEcs()
        {
            return this.repository.GetAll();
        }

        public IEnumerable<EC> FindEC(Expression<Func<EC, bool>> predicate)
        {
            return this.repository.Find(predicate);
        }

        /// <summary>
        /// Récupère la liste des EC qui ne sont pas affectés à des UE
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EC> GetECOrphelines()
        {
            return this.repository.Find(x => x.UE_ECID == null);
        }
    }
}
