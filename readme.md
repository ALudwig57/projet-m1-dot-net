Projet dot NET 2018/2019


Le  but  de  ce  projet  est  de  développer  une  application  d’aide  à  la  gestion  d’un département d’enseignement d’une composante de l’Université de Lorraine. Un exemple est le département informatique de l’UFR MIM.

Ce département gère des diplômes comme la Licence et le Master. La licence s’appuie sur 3 années (L1, L2, L3) et le master sur 2 annéesavec différentes orientations(M2GI, M2IHM,  etc).  Au  sein  de  ces  diplômes  des  enseignements  sont  à  assurer  par  des enseignants du départementinformatique. Le directeur du département doit s’assurer du service de chacun des membres du département en prévisionnel. Le service réaliséest à charge de la scolarité.

Il gère donc pour cela une liste de personnels et une liste d’enseignements. Un personnel possède au minimum un nom et un prénom. Il peut être de différentes catégoriesie. Maître  de  Conférences,  Professeur  des  Universités,  ATER,  PRAG,  PRCE,  etc.  Ces catégories  devront  être  paramétrables.  La  catégorie  indiquenotammentle  nombre d’heuresqu’il  doit  réaliser  pour  assurer  son  service.  Par  exemple  un  Maître  de Conférences doit assurer 192H d’enseignement en équivalent TD (EQTD) alors qu’un PRAG doit réaliser 384H d’enseignement en équivalent TD.

Un enseignement est en fait une UE avec un nom qui est éventuellement découpée en une ou plusieurs EC(un nom différent par EC). Chaque UE/EC s’appuie sur des heures présentielles pour les étudiants découpées en Cours magistraux (CM), Travaux Dirigés (TD) ou Travaux Pratiques (TP). Les CM sont pour l’ensemble de la promotion mais pour les travaux dirigés on précise le nombre de groupes, de même pour les TPs. Par exemple l’UE  «DotNet»  est  constituée  de  24CM  et deux  groupes  de  24TD.  Par  contre  si  un enseignant est attaché en tout ou partie à uneUE, l’équivalent TD (EQTD) est calculé selon différentes formules en fonction de sa catégorie. 

Par exemple pour un Maître de Conférences:
    -> 1 CM = 1,5 TD
    -> 1 TD =   1 TD
    -> 1 TP = 2/3 TD
 
Ces formules devront être paramétrables pour les différentes catégories.

Il existe également ce que l’on appelle l’Enseignement Intégré(EI). Dans ce cas, toujours pourun Maître de Conférences:
    -> 1 EI = 7/6 TD.
Pour les EI, on précise le nombre de groupes comme pour les TD et TP.

Le  directeur  du  département  doit  donc  pouvoir  décrire les  années  L1,  L2,  L3,  M1, M1APP, M2GI, M2 IHM, etc. Il doit pouvoir décrire des enseignements et les attacher à une année et un semestre. Il doit pouvoir décrire des catégories d’enseignants et décrire des  enseignants  en  s’appuyant  sur  ces  catégories.  Enfin  il  doit  pouvoir  attacher  des enseignements à ces enseignants. Par exemple, M. MARTIN aux 24CM et à un groupe 24TD de DotNet. Puis Mme PECCI au second groupe 24TD.M. MARTIN fait donc 60H EQTD et Mme PECCI fait 24 EQTD.

Il conviendra de tenir compte desaffectations déjà faites pour ne pas pouvoir affecter plus d’heures qu’il n’y a d’heures à faire dans une UE/EC.

Le  directeur  pourra  également  accéder  à  un  tableau  montrant  le  service  de  tous  les enseignants et du détail de chaque enseignant. Ce tableau de bord devra résumer au mieux  la  situation  d’un  enseignant  (sous-service,  sur-service,  etc.). En  plus  il  pourra accéder à un tableau de bord des diplômes, de toutes les annéeset semestres, du détail d’un diplôme, d’une année, d’un semestre ou d’unenseignement UE/EC.

Le client est à votre disposition pour des renseignements complémentaires.

Idéalement le projet devrait faire appel à des groupes de trois étudiants.

Vous êtes maitres des choix technologiques avec l’accord du client.
